<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SliderModule\Application\Model;

use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
use OxidEsales\Eshop\Core\Exception\DatabaseErrorException;
use OxidEsales\Eshop\Core\Model\BaseModel;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\TableViewNameGenerator;
use TheRealWorld\ToolsPlugin\Traits\DataGetter;

/**
 * Slider manager.
 * Performs Slider data/objects loading, deleting.
 */
class Slider extends BaseModel
{
    use DataGetter;

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_blUseLazyLoading = true;

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_sClassName = '\TheRealWorld\SliderModule\Application\Model\Slider';

    /** possible Classes that use Sliders */
    protected array $_aPossibleSliderClasses = [
        'Start', 'Article', 'Category', 'Footer',
    ];

    protected ?SliderElementList $_aSliderElements = null;

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function __construct()
    {
        parent::__construct();
        $this->init('trwslider');
    }

    /**
     * load slider object.
     *
     * @param string $sOxClass  - class name for slider
     * @param string $sObjectId - Object ID of class
     * @param string $sPos      - Position of Slider
     *
     * @throws DatabaseConnectionException
     */
    public function loadSlider(string $sOxClass, string $sObjectId = '', string $sPos = ''): bool
    {
        $result = false;
        if (in_array($sOxClass, $this->_aPossibleSliderClasses, true)) {
            $sSliderViewName = $this->getViewName();
            $viewNameGenerator = Registry::get(TableViewNameGenerator::class);
            $sViewName = $viewNameGenerator->getViewName('trwslider2object');

            $oDb = DatabaseProvider::getDb();
            $sSelect = "select {$sViewName}.`oxtrwsliderid`
                from {$sViewName}
                left join {$sSliderViewName} on ({$sViewName}.`oxtrwsliderid` = {$sSliderViewName}.`oxid`)
                where {$sViewName}.`oxclass` = " . $oDb->quote($sOxClass) . "
                and {$sSliderViewName}.`oxactive` = 1 ";

            if ($sObjectId) {
                $sSelect .= " and {$sViewName}.`oxobjectid` = " . $oDb->quote($sObjectId) . ' ';
            }

            if ($sPos) {
                $sSelect .= " and {$sViewName}.`oxpos` = " . $oDb->quote($sPos) . ' ';
            }

            $sSelect .= $this->_createConditionalSql();

            $sSelect .= 'limit 1';

            if ($sOxId = $oDb->getOne($sSelect)) {
                $result = $this->load($sOxId);
            }
        }

        return $result;
    }

    /** get all slider elements */
    public function getSliderElements(): ?SliderElementList
    {
        if (is_null($this->_aSliderElements) && $sOxId = $this->getId()) {
            $oTRWSliderElementList = oxNew(SliderElementList::class);
            $oTRWSliderElementList->loadElementsForSlider($sOxId, true);
            if ($oTRWSliderElementList->count()) {
                $this->_aSliderElements = $oTRWSliderElementList;
            }
        }

        return $this->_aSliderElements;
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     *
     * @throws DatabaseConnectionException
     * @throws DatabaseErrorException
     */
    public function delete($sOxId = null)
    {
        if (!$sOxId) {
            $sOxId = $this->getId();
        }
        if (!$sOxId) {
            return false;
        }

        $this->_deleteSliderElementRecords($sOxId);

        $this->_deleteSlider2ObjRecords($sOxId);

        if (parent::delete($sOxId)) {
            return true;
        }

        return false;
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     *
     * @throws DatabaseConnectionException
     */
    public function getSqlActiveSnippet($blForceCoreTable = null)
    {
        $oDb = DatabaseProvider::getDb();
        $sTable = $this->getViewName($blForceCoreTable);

        $sDate = date('Y-m-d H:i:s', Registry::getUtilsDate()->getTime());

        return " (
            {$sTable}.`oxactive` = 1 or
            (
                {$sTable}.`oxdatefrom` < " . $oDb->quote($sDate) . " or
                {$sTable}.`oxdatefrom` = null
            ) and
            (
                {$sTable}.`oxdateto` > " . $oDb->quote($sDate) . " or
                {$sTable}.`oxdateto` = null
            )
        )";
    }

    /**
     * Delete sliderelement records.
     *
     * @param string $sOxId Slider ID
     *
     * @throws DatabaseConnectionException
     * @throws DatabaseErrorException
     */
    protected function _deleteSliderElementRecords(string $sOxId): void
    {
        $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
        $viewNameGenerator = Registry::get(TableViewNameGenerator::class);
        $sViewName = $viewNameGenerator->getViewName('trwsliderelements', $this->getLanguage());

        // collect sliderelements to remove recursively
        $sSelect = "select `oxid`
            from {$sViewName}
            where oxtrwsliderid = " . $oDb->quote($sOxId);
        $oResults = $oDb->select($sSelect, false);
        $oSliderElements = oxNew(SliderElement::class);
        if ($oResults->count() > 0) {
            while (!$oResults->EOF) {
                $oSliderElements->setId($oResults->fields['oxid']);
                $oSliderElements->delete();
                $oResults->fetchRow();
            }
        }
    }

    /**
     * Delete slider2Object records.
     *
     * @param string $sOxId Slider ID
     *
     * @throws DatabaseErrorException
     * @throws DatabaseConnectionException
     */
    protected function _deleteSlider2ObjRecords(string $sOxId): void
    {
        $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
        $viewNameGenerator = Registry::get(TableViewNameGenerator::class);
        $sViewName = $viewNameGenerator->getViewName('trwslider2object');

        // collect sliderelements to remove recursively
        $sSelect = "select `oxid`
            from {$sViewName}
            where `oxtrwsliderid` = " . $oDb->quote($sOxId);
        $oResults = $oDb->select($sSelect, false);
        $oSlider2Object = oxNew(Slider2Object::class);
        if ($oResults->count() > 0) {
            while (!$oResults->EOF) {
                $oSlider2Object->setId($oResults->fields['oxid']);
                $oSlider2Object->delete();
                $oResults->fetchRow();
            }
        }
    }

    /** Helper Function to extend the Conditionals of Load SQLs */
    protected function _createConditionalSql(): string
    {
        return '';
    }
}
