<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SliderModule\Application\Model;

use Exception;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Field;
use OxidEsales\Eshop\Core\Model\BaseModel;
use TheRealWorld\ToolsPlugin\Core\ToolsDB;
use TheRealWorld\ToolsPlugin\Traits\DataGetter;

/**
 * Manages object sliderelement assignment to objects like articles.
 */
class SliderElement2Object extends BaseModel
{
    use DataGetter;

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_blUseLazyLoading = true;

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_sClassName = '\TheRealWorld\SliderModule\Application\Model\SliderElement2Object';

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function __construct()
    {
        parent::__construct();
        $this->init('trwsliderelements2object');
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function save()
    {
        $sOxId = ToolsDB::getAnyId(
            'trwsliderelements2object',
            [
                'oxobjectid'           => $this->getTRWStringData('oxobjectid'),
                'oxtrwsliderelementid' => $this->getTRWStringData('oxtrwsliderelementid'),
                'oxclass'              => $this->getTRWStringData('oxclass'),
            ]
        );

        // does not exist
        if (!$sOxId) {
            return parent::save();
        }
    }

    /** Load the Model bei SliderElementId */
    public function loadBySliderElementId(string $sSliderElementId): bool
    {
        $bResult = false;
        if (
            $sSliderElementId
            && ($sOxId = ToolsDB::getAnyId(
                'trwsliderelements2object',
                ['oxtrwsliderelementid' => $sSliderElementId]
            ))
        ) {
            $bResult = $this->load($sOxId);
        }

        return $bResult;
    }

    /**
     * Delete all connections between Element and Objects.
     *
     * @param string $sSliderElementId ID of Slider Element
     * @param string $sObjId           Object ID (optional)
     *
     * @throws Exception
     */
    public function cleanElement2Object(string $sSliderElementId, string $sObjId = ''): void
    {
        if (!$sSliderElementId) {
            return;
        }
        $sSliderElement2ObjTable = 'trwsliderelements2object';
        $oDb = DatabaseProvider::getDb();
        $sQ = "delete from {$sSliderElement2ObjTable}
            where `oxtrwsliderelementid` = " . $oDb->quote($sSliderElementId);
        if ($sObjId) {
            $sQ .= 'and `oxobjectid` not in (' . $oDb->quote($sObjId) . ') ';
        }
        ToolsDB::execute($sQ);

        // if we have a ObjId, we delete the extern Linktarget
        if ($sObjId) {
            $oTRWSliderElement = oxNew(SliderElement::class);
            if ($oTRWSliderElement->load($sSliderElementId)) {
                $oTRWSliderElement->trwsliderelements__oxlink = new Field('');
                $oTRWSliderElement->save();
            }
        }
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function delete($sOxId = null)
    {
        if (!$sOxId) {
            $sOxId = $this->getId();
        }
        if (!$sOxId) {
            return false;
        }

        if (parent::delete($sOxId)) {
            return true;
        }

        return false;
    }
}
