<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SliderModule\Application\Model;

use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
use OxidEsales\Eshop\Core\Model\ListModel;

/**
 * SliderElementList class.
 */
class SliderElementList extends ListModel
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_sObjectsInListName = 'TheRealWorld\SliderModule\Application\Model\SliderElement';

    /**
     * Load Elements for Slider.
     *
     * @param string $sSliderId  Slider OxId
     * @param bool   $bCheckDate should the date to be considered?
     *
     * @throws DatabaseConnectionException
     */
    public function loadElementsForSlider(string $sSliderId, bool $bCheckDate = false): void
    {
        $oBaseObject = $this->getBaseObject();
        $sElementTable = $oBaseObject->getViewName();
        $sElementFields = $oBaseObject->getSelectFields();

        $oDb = DatabaseProvider::getDb();
        $sSql = "select {$sElementFields}
            from {$sElementTable}
            where {$sElementTable}.`oxtrwsliderid` = " . $oDb->quote($sSliderId) . ' ';

        if ($bCheckDate) {
            $sSql .= "and ({$sElementTable}.`oxdatefrom` = 0 or {$sElementTable}.`oxdatefrom` < now()) and
                ({$sElementTable}.`oxdateto` = 0 or {$sElementTable}.`oxdateto` > now()) ";
        }

        $sSql .= "and {$sElementTable}.`oxactive` = '1'
            order by {$sElementTable}.`oxsort`";

        $this->selectString($sSql);
    }

    /**
     * Load Ids for Slider Admin.
     *
     * @param string $sSliderId Slider OxId
     *
     * @throws DatabaseConnectionException
     */
    public function loadElementsForSliderAdmin(string $sSliderId): void
    {
        $oBaseObject = $this->getBaseObject();
        $sElementTable = $oBaseObject->getViewName();
        $sElementFields = $oBaseObject->getSelectFields();

        $oDb = DatabaseProvider::getDb();
        $sSql = "select {$sElementFields}
            from {$sElementTable}
            where {$sElementTable}.`oxtrwsliderid` = " . $oDb->quote($sSliderId) . "
            order by {$sElementTable}.`oxsort`";

        $this->selectString($sSql);
    }
}
