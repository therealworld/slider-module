<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SliderModule\Application\Model;

use OxidEsales\Eshop\Core\Model\MultiLanguageModel;
use OxidEsales\Eshop\Core\Registry;
use TheRealWorld\ToolsPlugin\Traits\DataGetter;

/**
 * Slider Element manager.
 * Performs Sliderelement data/objects loading, deleting.
 */
class SliderElement extends MultiLanguageModel
{
    use DataGetter;

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_blUseLazyLoading = true;

    /** SliderPictureUrl */
    protected ?string $_sSliderPictureUrl = null;

    /** SliderPictureAlternativeUrl */
    protected ?string $_sSliderPictureAlternativeUrl = null;

    /** SliderLink */
    protected ?string $_sLink = null;

    /** SliderLinkClass */
    protected ?string $_sLinkClass = null;

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_sClassName = '\TheRealWorld\SliderModule\Application\Model\SliderElement';

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function __construct()
    {
        parent::__construct();
        $this->init('trwsliderelements');
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function delete($sOxId = null)
    {
        if (!$sOxId) {
            $sOxId = $this->getId();
        }
        if (!$sOxId) {
            return false;
        }

        $this->deletePic();
        $this->deletePicAlternative();

        if (parent::delete($sOxId)) {
            return true;
        }

        return false;
    }

    /** Delete Pic */
    public function deletePic(): void
    {
        $this->_deletePic('oxpic');
    }

    /** Delete Pic Alternative */
    public function deletePicAlternative(): void
    {
        $this->_deletePic('oxpicalternative');
    }

    /**
     * Returns Slider Image.
     *
     * @param bool $bSsl to force SSL
     */
    public function getSliderPictureUrl(bool $bSsl = true): string
    {
        if (is_null($this->_sSliderPictureUrl)) {
            $this->_sSliderPictureUrl = $this->_sliderPictureUrl(
                'slider/',
                'oxpic',
                'sTRWSliderPicSize',
                $bSsl
            );
        }

        return $this->_sSliderPictureUrl;
    }

    /**
     * Returns alternative Slider Image.
     *
     * @param bool $bSsl to force SSL
     */
    public function getSliderPictureAlternativeUrl(bool $bSsl = true): string
    {
        if (is_null($this->_sSliderPictureAlternativeUrl)) {
            $this->_sSliderPictureAlternativeUrl = $this->_sliderPictureUrl(
                'slider_alternative/',
                'oxpicalternative',
                'sTRWSliderPicAlternativeSize',
                $bSsl
            );
        }

        return $this->_sSliderPictureAlternativeUrl;
    }

    /** Returns SliderImage Link */
    public function getLink(): string
    {
        $this->_prepareLink();

        return $this->_sLink;
    }

    /** Returns SliderImage LinkClass */
    public function getLinkClass(): string
    {
        $this->_prepareLink();

        return $this->_sLinkClass;
    }

    /** Get is Fullwidth */
    public function isFullWidth(): bool
    {
        $bResult = false;
        if ($this->getId()) {
            $bResult = (bool) $this->getTRWStringData('oxfullwidth');
        }

        return $bResult;
    }

    /** Get Background-Color */
    public function getBGColor(): string
    {
        $sResult = '';
        if ($this->getId()) {
            $sResult = $this->getTRWStringData('oxbgcolor');
        }

        return $sResult;
    }

    /** Get Button-Color */
    public function getBtnColor(): string
    {
        $sResult = '';
        if ($this->getId()) {
            $sResult = $this->getTRWStringData('oxbtncolor');
        }

        return $sResult;
    }

    /** Get Button-Background-Color */
    public function getBtnBGColor(): string
    {
        $sResult = '';
        if ($this->getId()) {
            $sResult = $this->getTRWStringData('oxbtnbgcolor');
        }

        return $sResult;
    }

    /** Get Title */
    public function getTitle(): string
    {
        $sResult = '';
        if ($this->getId()) {
            $sResult = $this->getTRWStringData('oxtitle');
        }

        return $sResult;
    }

    /** Get Link-Target */
    public function getLinkTarget(): string
    {
        $sResult = '';
        if ($this->getId()) {
            $sResult = $this->getTRWStringData('oxlinktarget');
        }

        return $sResult;
    }

    /** Get Image Title */
    public function getImageTitle(): string
    {
        $sResult = '';
        if ($this->getId()) {
            $sResult = $this->getTRWStringData('oxpictitle');
        }

        return $sResult;
    }

    /** Get Description */
    public function getDescription(): string
    {
        $sResult = '';
        if ($this->getId()) {
            $sResult = $this->getTRWRawStringData('oxdesc');
        }

        return $sResult;
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function save()
    {
        // if we have a extern link, we delete the connections ...
        if ($this->getTRWStringData('oxlink')) {
            $oTRWSliderElement2Object = oxNew(SliderElement2Object::class);
            $oTRWSliderElement2Object->cleanElement2Object($this->getId());
        }

        return parent::save();
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function getSqlActiveSnippet($blForceCoreTable = null)
    {
        $sTable = $this->getViewName($blForceCoreTable);

        $sQ = " {$sTable}.oxactive = 1 ";

        $sDate = date('Y-m-d H:i:s', Registry::getUtilsDate()->getTime());

        return " ( {$sQ} or ( {$sTable}.oxdatefrom < '{$sDate}' or {$sTable}.oxdatefrom = 0) and
             ( {$sTable}.oxdateto > '{$sDate}' or {$sTable}.oxdateto = 0) ) ";
    }

    protected function _deletePic($sField): void
    {
        $sImage = basename($this->getTRWStringData($sField));
        if ($sImage && $sImage !== 'nopic.jpg') {
            Registry::getUtilsPic()->safePictureDelete(
                $sImage,
                Registry::getConfig()->getPictureDir(false) .
                Registry::getUtilsFile()->normalizeSliderPictureAlternativeDir(),
                'trwsliderelements',
                $sField
            );
        }
    }

    /**
     * Returns Slider Image.
     *
     * @param bool $bSsl to force SSL
     */
    protected function _sliderPictureUrl(string $sPath, string $sField, string $sConfigName, bool $bSsl = true): string
    {
        return (string) Registry::getPictureHandler()->getPicUrl(
            $sPath,
            basename($this->getTRWStringData($sField)),
            Registry::getConfig()->getConfigParam($sConfigName),
            null,
            null,
            $bSsl
        );
    }

    /** Helper Method to create Link */
    protected function _prepareLink(): void
    {
        if (is_null($this->_sLink)) {
            $this->_sLink = '';
            $this->_sLinkClass = '';
            if ($sOxId = $this->getId()) {
                if ($this->getTRWStringData('oxlink')) {
                    $this->_sLink = $this->getTRWStringData('oxlink');
                } else {
                    $oTRWSliderElement2Object = oxNew(SliderElement2Object::class);
                    if (
                        $oTRWSliderElement2Object->loadBySliderElementId($sOxId)
                        && $oTRWSliderElement2Object->getTRWStringData('oxclass')
                    ) {
                        $oObject = oxNew(
                            '\\OxidEsales\\Eshop\\Application\\Model\\' .
                            $oTRWSliderElement2Object->getTRWStringData('oxclass')
                        );
                        if ($oObject->load($oTRWSliderElement2Object->getTRWStringData('oxobjectid'))) {
                            $this->_sLink = $oObject->getLink();
                            $this->_sLinkClass = $oTRWSliderElement2Object->getTRWStringData('oxclass');
                        }
                    }
                }
            }
        }
    }
}
