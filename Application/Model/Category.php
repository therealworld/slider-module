<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SliderModule\Application\Model;

use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
use TheRealWorld\ToolsPlugin\Core\ToolsDB;
use TheRealWorld\ToolsPlugin\Traits\DataGetter;

/**
 * Slider Category class.
 *
 * @mixin \OxidEsales\Eshop\Application\Model\Category
 */
class Category extends Category_parent
{
    use DataGetter;

    /**
     * OXID-Core.
     * {@inheritDoc}
     *
     * @throws DatabaseConnectionException
     */
    public function delete($sOxId = null)
    {
        if (!$sOxId) {
            $sOxId = $this->getId();
        }

        $bResult = parent::delete($sOxId);

        $oDb = DatabaseProvider::getDb();

        $sSlider2ObjTable = 'trwslider2object';
        $sDelete = "delete from {$sSlider2ObjTable}
            where `oxobjectid` = " . $oDb->quote($sOxId) . "
            and `oxclass` = 'Category'";
        ToolsDB::execute($sDelete);

        $sSliderElement2ObjTable = 'trwsliderelements2object';
        $sDelete = "delete from {$sSliderElement2ObjTable}
            where `oxobjectid` = " . $oDb->quote($sOxId) . "
            and oxclass = 'Category'";
        ToolsDB::execute($sDelete);

        return $bResult;
    }
}
