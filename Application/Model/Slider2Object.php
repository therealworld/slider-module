<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SliderModule\Application\Model;

use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
use OxidEsales\Eshop\Core\Model\BaseModel;
use TheRealWorld\ToolsPlugin\Core\ToolsDB;
use TheRealWorld\ToolsPlugin\Traits\DataGetter;

/**
 * Manages object slider assignment to objects like articles.
 */
class Slider2Object extends BaseModel
{
    use DataGetter;

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_blUseLazyLoading = true;

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_sClassName = '\TheRealWorld\SliderModule\Application\Model\Slider2Object';

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function __construct()
    {
        parent::__construct();
        $this->init('trwslider2object');
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function save()
    {
        $sOxId = ToolsDB::getAnyId(
            'trwslider2object',
            [
                'oxobjectid'    => $this->getTRWStringData('oxobjectid'),
                'oxtrwsliderid' => $this->getTRWStringData('oxtrwsliderid'),
                'oxclass'       => $this->getTRWStringData('oxclass'),
                'oxpos'         => $this->getTRWStringData('oxpos'),
            ]
        );

        // does not exist
        if (!$sOxId) {
            return parent::save();
        }
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function delete($sOxId = null)
    {
        if (is_null($sOxId)) {
            $sOxId = $this->getId();
        }
        if (!$sOxId) {
            return false;
        }

        if (parent::delete($sOxId)) {
            return true;
        }

        return false;
    }

    /**
     * Delete all connections except $sOxId.
     *
     * @param string $sClass    Class of connections
     * @param string $sSliderId id of Slider
     * @param string $sPos      positition of slider
     *
     * @throws DatabaseConnectionException
     */
    public function cleanConnections(string $sClass = 'Start', string $sSliderId = '', string $sPos = ''): void
    {
        if ($sClass && $sSliderId) {
            $oDb = DatabaseProvider::getDb();

            $sSlider2ObjTable = 'trwslider2object';
            $sQ = "delete from {$sSlider2ObjTable}
                where `oxtrwsliderid` = " . $oDb->quote($sSliderId) . '
                and `oxclass` != ' . $oDb->quote($sClass);

            if ($sPos) {
                $sQ .= ' and `oxpos` != ' . $oDb->quote($sPos);
            }

            ToolsDB::execute($sQ);
        }
    }
}
