[{include file="headitem.tpl" title="TRWSLIDER_TEMPLATE_TITLE"|oxmultilangassign box="list"}]
[{assign var="where" value=$oView->getListFilter()}]

[{if $readonly}]
    [{assign var="readonly" value="readonly disabled"}]
[{else}]
    [{assign var="readonly" value=""}]
[{/if}]

<script type="text/javascript">
    <!--
    window.onload = function ()
    {
        top.reloadEditFrame();
        [{if $updatelist == 1}]
            top.oxid.admin.updateList('[{$oxid}]');
        [{/if}]
    }
    //-->
</script>

<div id="liste">
    <form name="search" id="search" action="[{$oViewConf->getSelfLink()}]" method="post">
        [{include file="_formparams.tpl" cl="sliderlist" lstrt=$lstrt actedit=$actedit oxid=$oxid fnc="" language=$actlang editlanguage=$actlang}]
        <table cellspacing="0" cellpadding="0" border="0" width="100%">
            <colgroup>
                [{block name="admin_trwslider_list_colgroup"}]
                    <col width="3%" />
                    <col width="46%" />
                    <col width="46%" />
                    <col width="5%" />
                [{/block}]
            </colgroup>
            <tr class="listfilter">
                [{block name="admin_trwslider_list_filter"}]
                    <td valign="top" class="listfilter first" height="20">
                    </td>
                    <td valign="top" class="listfilter">
                        <div class="r1">
                            <div class="b1">
                                <input class="listedit" type="text" size="20" maxlength="128" name="where[trwslider][oxdatefrom]" value="[{$where.trwslider.oxdatefrom}]" />
                            </div>
                        </div>
                    </td>
                    <td valign="top" class="listfilter">
                        <div class="r1">
                            <div class="b1">
                                <input class="listedit" type="text" size="20" maxlength="128" name="where[trwslider][oxtitle]" value="[{$where.trwslider.oxtitle}]" />
                            </div>
                        </div>
                    </td>
                    <td valign="top" class="listfilter">
                        <div class="r1">
                            <div class="b1">
                                <div class="find">
                                    <input class="listedit" type="submit" name="submitit" value="[{oxmultilang ident="GENERAL_SEARCH"}]" />
                                </div>
                            </div>
                        </div>
                    </td>
                [{/block}]
            </tr>
            <tr>
                [{block name="admin_trwslider_list_sorting"}]
                    <td class="listheader first" height="15" width="30" align="center">
                        <a href="#" onclick="top.oxid.admin.setSorting(document.search, 'trwslider', 'oxactive', 'asc'); document.search.submit(); return false;" class="listheader">[{oxmultilang ident="GENERAL_ACTIVTITLE"}]</a>
                    </td>
                    <td class="listheader">
                        <a href="#" onclick="top.oxid.admin.setSorting(document.search, 'trwslider', 'oxdatefrom', 'asc'); document.search.submit(); return false;" class="listheader">[{oxmultilang ident="GENERAL_DATE"}]</a>
                    </td>
                    <td class="listheader">
                        <a href="#" onclick="top.oxid.admin.setSorting(document.search, 'trwslider', 'oxtitle', 'asc'); document.search.submit(); return false;" class="listheader">[{oxmultilang ident="GENERAL_NAME"}]</a>
                    </td>
                    <td class="listheader">
                    </td>
                [{/block}]
            </tr>

            [{assign var="blWhite" value=""}]
            [{assign var="_cnt" value=0}]
            [{foreach from=$mylist item=listitem}]
                [{assign var="_cnt" value=$_cnt+1}]
                <tr id="row.[{$_cnt}]">
                    [{block name="admin_trwslider_list_item"}]
                        [{if $listitem->blacklist == 1}]
                            [{assign var="listclass" value=listitem3}]
                        [{else}]
                            [{assign var="listclass" value=listitem$blWhite}]
                        [{/if}]
                        [{if $listitem->getId() == $oxid}]
                            [{assign var="listclass" value=listitem4}]
                        [{/if}]
                        <td valign="top" class="[{$listclass}][{if $listitem->trwslider__oxactive->value == 1}] active[{/if}]" height="15">
                            <div class="listitemfloating">&nbsp;</div>
                        </td>
                        <td valign="top" class="[{$listclass}]">
                            <div class="listitemfloating">
                                <a href="#" onclick="top.oxid.admin.editThis('[{$listitem->trwslider__oxid->value}]'); return false;" class="[{$listclass}]">[{$listitem->trwslider__oxdatefrom->value|oxformdate}]</a>
                            </div>
                        </td>
                        <td valign="top" class="[{$listclass}]">
                            <div class="listitemfloating">
                                <a href="#" onclick="top.oxid.admin.editThis('[{$listitem->trwslider__oxid->value}]'); return false;" class="[{$listclass}]">[{$listitem->trwslider__oxtitle->value}]</a>
                            </div>
                        </td>
                        <td class="[{$listclass}]">
                            [{if !$readonly}]
                                <a href="#" onclick="top.oxid.admin.deleteThis('[{$listitem->trwslider__oxid->value}]'); return false;" class="delete" id="del.[{$_cnt}]" title="" [{include file="help.tpl" helpid=item_delete}]></a>
                            [{/if}]
                        </td>
                    [{/block}]
                </tr>
                [{if $blWhite == "2"}]
                    [{assign var="blWhite" value=""}]
                [{else}]
                    [{assign var="blWhite" value="2"}]
                [{/if}]
            [{/foreach}]
            [{include file="pagenavisnippet.tpl" colspan="4"}]
        </table>
    </form>
</div>

[{include file="pagetabsnippet.tpl"}]

<script type="text/javascript">
    if (parent.parent)
    {
        parent.parent.sShopTitle   = "[{$actshopobj->oxshops__oxname->getRawValue()|oxaddslashes}]";
        parent.parent.sMenuItem    = "[{oxmultilang ident='GENERAL_MENUITEM'}]";
        parent.parent.sMenuSubItem = "[{oxmultilang ident='TRWSLIDER_TEMPLATE_TITLE'}]";
        parent.parent.sWorkArea    = "[{$_act}]";
        parent.parent.setTitle();
    }
</script>
</body>
</html>
