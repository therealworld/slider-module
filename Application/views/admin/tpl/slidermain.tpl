[{include file="headitem.tpl" title="TRWSLIDER_TEMPLATE_TITLE"|oxmultilangassign}]

<script type="text/javascript">
    <!--
    function checkValue(el, sTargetIdent, sDisplayStyle = 'table-row')
    {
        var oTargetEl = document.getElementById(sTargetIdent);
        if (el.checked)
        {
            oTargetEl.style.display = sDisplayStyle;
        }
        else
        {
            oTargetEl.style.display = "none";
        }
        return false;
    }
    -->
</script>

[{if $readonly}]
    [{assign var="readonly" value="readonly disabled"}]
[{else}]
    [{assign var="readonly" value=""}]
[{/if}]
[{assign var="bShowDynamic" value=false}]
[{if ($oViewConf->getConfigParam('bTRWSliderShowCtrlArticle') || $oViewConf->getConfigParam('bTRWSliderShowCtrlCategory') || $oViewConf->getConfigParam('bTRWSliderShowCtrlContent'))}]
    [{assign var="bShowDynamic" value=true}]
[{/if}]
<form name="transfer" id="transfer" action="[{$oViewConf->getSelfLink()}]" method="post">
    [{$oViewConf->getHiddenSid()}]
    <input type="hidden" name="oxid" value="[{$oxid}]" />
    <input type="hidden" name="cl" value="slidermain" />
    <input type="hidden" name="editlanguage" value="[{$editlanguage}]" />
</form>

<form name="myedit" id="myedit" action="[{$oViewConf->getSelfLink()}]" method="post">
    [{$oViewConf->getHiddenSid()}]
    <input type="hidden" name="cl" value="slidermain" />
    <input type="hidden" name="fnc" value="" />
    <input type="hidden" name="oxid" value="[{$oxid}]" />
    <input type="hidden" name="editval[trwslider__oxid]" value="[{$oxid}]" />
    <input type="hidden" name="staticposition" value="" />
    [{if (!$oCtrlStartPosValues || $oCtrlStartPosValues|@count == 2)}]
        <input type="hidden" name="sstartpagepos" value="[{$sCtrlStartPosDefault}]" />
    [{/if}]
    <table cellspacing="0" cellpadding="0" border="0" width="98%">
        <tr>
            <td valign="top" class="edittext">

                <table cellspacing="0" cellpadding="0" border="0">
                    [{block name="admin_trwslider_main_form"}]
                        <tr>
                            <td class="edittext" width="90">
                                [{oxmultilang ident="GENERAL_ACTIVE"}]
                            </td>
                            <td class="edittext">
                                <input type="hidden" name="editval[trwslider__oxactive]" value="0" />
                                <input class="edittext" type="checkbox" name="editval[trwslider__oxactive]" value="1" [{if $edit->trwslider__oxactive->value == 1}]checked[{/if}] [{$readonly}] />
                                [{oxinputhelp ident="HELP_GENERAL_ACTIVE"}]
                            </td>
                        </tr>

                        <tr>
                            <td class="edittext">
                                [{oxmultilang ident="GENERAL_NAME"}]
                            </td>
                            <td class="edittext">
                                <input type="text" class="editinput" size="25" maxlength="[{$edit->trwslider__oxtitle->fldmax_length}]" name="editval[trwslider__oxtitle]" value="[{$edit->trwslider__oxtitle->value}]" [{$readonly}] />
                                [{oxinputhelp ident="HELP_GENERAL_NAME"}]
                            </td>
                        </tr>

                        <tr>
                            <td class="edittext">
                                [{oxmultilang ident="GENERAL_DATE"}]
                            </td>
                            <td class="edittext">
                            [{oxmultilang ident="GENERAL_FROM"}]<input type="text" class="editinput" size="30" maxlength="[{$edit->trwslider__oxdatefrom->fldmax_length}]" name="editval[trwslider__oxdatefrom]" value="[{$edit->trwslider__oxdatefrom|oxformdate}]" [{include file="help.tpl" helpid=article_vonbis}] [{$readonly}] /><br />
                            [{oxmultilang ident="GENERAL_TILL"}] <input type="text" class="editinput" size="30" maxlength="[{$edit->trwslider__oxdateto->fldmax_length}]" name="editval[trwslider__oxdateto]" value="[{$edit->trwslider__oxdateto|oxformdate}]" [{include file="help.tpl" helpid=article_vonbis}] [{$readonly}] />
                            [{oxinputhelp ident="HELP_GENERAL_DATE"}]
                            </td>
                        </tr>

                    [{/block}]
                    <tr>
                        <td class="edittext">
                        </td>
                        <td class="edittext"><br />
                            <input type="submit" class="edittext" name="save" value="[{oxmultilang ident="GENERAL_SAVE"}]" onclick="document.myedit.fnc.value='save'" [{$readonly}] />
                        </td>
                    </tr>
                </table>
            </td>

            <td valign="top" class="edittext" align="left" width="50%">
                <table cellspacing="0" cellpadding="0" border="0">
                    [{block name="admin_trwslider_connect"}]
                        [{if $oxid != "-1"}]
                            [{if $oViewConf->getConfigParam('bTRWSliderShowCtrlStart')}]
                                <tr>
                                    <td class="edittext" colspan="2">
                                        <h3>[{oxmultilang ident="TRWSLIDER_ASSIGNSTARTPAGE"}]</h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="edittext" width="90">
                                        [{oxmultilang ident="TRWSLIDER_STARTPAGE"}]
                                    </td>
                                    <td class="edittext">
                                        <input class="edittext" type="radio" name="staticposition" value="Start" [{if $bOnStartPage}]checked[{/if}] [{$readonly}] onclick="checkValue(this, 'sliderPosSelect');" />
                                        [{oxinputhelp ident="HELP_TRWSLIDER_STARTPAGE"}]
                                    </td>
                                </tr>
                                <tr id="sliderPosSelect" [{if !$bOnStartPage}]style="display:none;"[{/if}]>
                                    [{if ($oCtrlStartPosValues && $oCtrlStartPosValues|@count >= 2)}]
                                        <td class="edittext" width="90">
                                            [{oxmultilang ident="TRWSLIDER_OXPOS"}] [{oxmultilang ident="TRWSLIDER_STARTPAGE"}]
                                        </td>
                                        <td class="edittext">
                                            <select name="sstartpagepos">
                                                [{foreach from=$oCtrlStartPosValues item=aItem key=iKey}]
                                                    <option value="[{$aItem->value}]" [{if $aItem->selected}]selected[{/if}]>[{$aItem->title}]</option>
                                                [{/foreach}]
                                            </select>
                                            [{oxinputhelp ident="HELP_TRWSLIDER_OXPOS"}]
                                        </td>
                                    [{/if}]
                                </tr>
                            [{/if}]
                            [{if $oViewConf->getConfigParam('bTRWSliderShowCtrlFooter')}]
                                <tr>
                                    <td class="edittext" colspan="2">
                                        <h3>[{oxmultilang ident="TRWSLIDER_ASSIGNFOOTER"}]</h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="edittext" width="90">
                                        [{oxmultilang ident="TRWSLIDER_FOOTER"}]
                                    </td>
                                    <td class="edittext">
                                        <input class="edittext" type="radio" name="staticposition" value="Footer" [{if $bOnFooter}]checked[{/if}] [{$readonly}] />
                                        [{oxinputhelp ident="HELP_TRWSLIDER_FOOTER"}]
                                    </td>
                                </tr>
                            [{/if}]
                            [{if $bShowDynamic}]
                                <tr>
                                    <td class="edittext" colspan="2">
                                        <h3>[{oxmultilang ident="TRWSLIDER_ASSIGN"}]</h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="edittext" colspan="2">
                                        [{if $oViewConf->getConfigParam('bTRWSliderShowCtrlArticle')}]
                                            <input [{$readonly}] type="button" value="[{oxmultilang ident="GENERAL_ASSIGNARTICLE"}]" class="edittext" onclick="showDialog('&cl=SliderMain&aoc=1&oxid=[{$oxid}]');"><br />
                                        [{/if}]
                                        [{if $oViewConf->getConfigParam('bTRWSliderShowCtrlCategory')}]
                                            <input [{$readonly}] type="button" value="[{oxmultilang ident="GENERAL_ASSIGNCATEGORIES"}]" class="edittext" onclick="showDialog('&cl=SliderMain&aoc=2&oxid=[{$oxid}]');"><br />
                                        [{/if}]
                                        [{if $oViewConf->getConfigParam('bTRWSliderShowCtrlContent')}]
                                            <input [{$readonly}] type="button" value="[{oxmultilang ident="GENERAL_ASSIGNCONTENT"}]" class="edittext" onclick="showDialog('&cl=SliderMain&aoc=3&oxid=[{$oxid}]');"><br />
                                        [{/if}]
                                    </td>
                                </tr>
                            [{/if}]
                        [{/if}]
                    [{/block}]
                </table>
            </td>
        </tr>
    </table>

</form>
[{include file="bottomnaviitem.tpl"}]
[{include file="bottomitem.tpl"}]