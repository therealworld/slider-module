[{include file="popups/headitem.tpl" title="GENERAL_ADMIN_TITLE"|oxmultilangassign}]

<script type="text/javascript">
    <!--
    initAoc = function()
    {
        [{assign var="sSep" value=""}]

        YAHOO.oxid.container1 = new YAHOO.oxid.aoc('container1',
            [ [{foreach from=$oxajax.container1 item=aItem key=iKey}]
            [{$sSep}][{strip}]{ key:'_[{$iKey}]', ident: [{if $aItem.4}]true[{else}]false[{/if}]
                [{if !$aItem.4}],
                label: '[{oxmultilang ident="GENERAL_AJAX_SORT_"|cat:$aItem.0|oxupper}]',
                visible: [{if $aItem.2}]true[{else}]false[{/if}]
                [{/if}]}
                [{/strip}]
                [{assign var="sSep" value=","}]
            [{/foreach}] ],
            '[{$oViewConf->getAjaxLink()}]cmpid=container1&container=SliderContent&synchoxid=[{$oxid}]'
        );

        [{assign var="sSep" value=""}]

        YAHOO.oxid.container2 = new YAHOO.oxid.aoc('container2',
            [ [{foreach from=$oxajax.container2 item=aItem key=iKey}]
            [{$sSep}][{strip}]{ key:'_[{$iKey}]', ident: [{if $aItem.4}]true[{else}]false[{/if}]
                [{if !$aItem.4}],
                label: '[{oxmultilang ident="GENERAL_AJAX_SORT_"|cat:$aItem.0|oxupper}]',
                visible: [{if $aItem.2}]true[{else}]false[{/if}]
                [{/if}]}
                [{/strip}]
                [{assign var="sSep" value=","}]
            [{/foreach}] ],
            '[{$oViewConf->getAjaxLink()}]cmpid=container2&container=SliderContent&oxid=[{$oxid}]'
        );

        YAHOO.oxid.container1.getDropAction = function()
        {
            return 'fnc=addContentToSlider';
        }

        YAHOO.oxid.container2.getDropAction = function()
        {
            return 'fnc=removeContentFromSlider';
        }

        YAHOO.oxid.container2.subscribe("rowClickEvent", function(oParam)
        {
            var aSelRows= YAHOO.oxid.container2.getSelectedRows();
            if (aSelRows.length)
            {
                oParam = YAHOO.oxid.container2.getRecord(aSelRows[0]);
                $('_contentname').innerHTML = oParam._oData._0;
                $('trwslider2object_oxid').value = oParam._oData._2;
                $('slider_pos').value = oParam._oData._3;
                $('trwslider2object_oxtrwsliderid').value = oParam._oData._4;
                $('trwslider2object_oxobjectid').value = oParam._oData._1;
                $D.setStyle($('slider_conf'), 'visibility', '');

                var callback = {
                    success: YAHOO.oxid.container2.onShow,
                    failure: YAHOO.oxid.container2.onFailure
                };
                YAHOO.util.Connect.asyncRequest('GET', '[{$oViewConf->getAjaxLink()}]&cmpid=container2&container=SliderContent&fnc=getPosValuesAsHtmlOptionForSelect&sclass=oxcontent&oxtrwsliderid=' + encodeURIComponent($('trwslider2object_oxtrwsliderid').value) + '&oxobjectid=' + encodeURIComponent($('trwslider2object_oxobjectid').value), callback);
            }
            else
            {
                $D.setStyle($('slider_conf'), 'visibility', 'hidden');
            }
        });

        YAHOO.oxid.container2.subscribe("dataReturnEvent", function()
        {
            $D.setStyle($('slider_conf'), 'visibility', 'hidden');
        });

        YAHOO.oxid.container2.onShow = function(res)
        {
            $('slider_pos').innerHTML = res.responseText;
        }
        YAHOO.oxid.container2.onSave = function()
        {
            YAHOO.oxid.container1.getDataSource().flushCache();
            YAHOO.oxid.container1.getPage(0);
            YAHOO.oxid.container2.getDataSource().flushCache();
            YAHOO.oxid.container2.getPage(0);
        }
        YAHOO.oxid.container2.onFailure = function() { /* currently does nothing */ }

        YAHOO.oxid.container2.saveSlider = function()
        {
            var callback = {
                success: YAHOO.oxid.container2.onSave,
                failure: YAHOO.oxid.container2.onFailure,
                scope: YAHOO.oxid.container2
            };
            YAHOO.util.Connect.asyncRequest('GET', '[{$oViewConf->getAjaxLink()}]&cmpid=container2&container=SliderContent&fnc=saveSliderValue&oxid=[{$oxid}]&slider_pos=' + encodeURIComponent($('slider_pos').value) + '&trwslider2object_oxid=' + encodeURIComponent($('trwslider2object_oxid').value), callback);
        }

        // subscribint event listeners on buttons
        $E.addListener($('saveBtn'), "click", YAHOO.oxid.container2.saveSlider, $('saveBtn'));
    }
    $E.onDOMReady(initAoc);
    -->
</script>

<table width="100%">
    <colgroup>
        <col span="2" width="40%" />
        <col width="20%" />
    </colgroup>
    <tr class="edittext">
        <td colspan="3">[{oxmultilang ident="GENERAL_AJAX_DESCRIPTION"}]<br/>[{oxmultilang ident="GENERAL_FILTERING"}]<br /><br /></td>
    </tr>
    <tr class="edittext">
        <td align="center"><b>[{oxmultilang ident="CMSFOLDER_USERINFO"}]</b></td>
        <td align="center"><b>[{oxmultilang ident="TRWSLIDER_ASSIGNEDCONTENTS"}]</b></td>
        <td align="center"><b>[{oxmultilang ident="TRWSLIDER_SELECTONECONTENT"}]</b></td>
    </tr>
    <tr>
        <td valign="top" id="container1"></td>
        <td valign="top" id="container2"></td>
        <td valign="top" align="center" class="edittext" id="slider_conf" style="visibility:hidden">
            <br/><br/>
            <b id="_contentname"></b>:<br/><br/>
            <input id="trwslider2object_oxtrwsliderid" type="hidden" />
            <input id="trwslider2object_oxobjectid" type="hidden" />
            <input id="trwslider2object_oxid" type="hidden" />
            [{oxmultilang ident="TRWSLIDER_OXPOS"}]:<br/>
            <select id="slider_pos"></select>
            <br/><br/>
            <input id="saveBtn" type="button" class="edittext" value="[{oxmultilang ident="GENERAL_SAVE"}]" />
        </td>
    </tr>
    <tr>
        <td class="oxid-aoc-actions">
            <input type="button" value="[{oxmultilang ident="GENERAL_AJAX_ASSIGNALL"}]" id="container1_btn" />
        </td>
        <td class="oxid-aoc-actions">
            <input type="button" value="[{oxmultilang ident="GENERAL_AJAX_UNASSIGNALL"}]" id="container2_btn" />
        </td>
        <td>&nbsp;</td>
    </tr>
</table>
</body>
</html>
