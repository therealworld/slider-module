[{include file="popups/headitem.tpl" title="GENERAL_ADMIN_TITLE"|oxmultilangassign}]

<script type="text/javascript">
    <!--
    initAoc = function()
    {
        [{assign var="sSep" value=""}]

        YAHOO.oxid.container1 = new YAHOO.oxid.aoc('container1',
            [ [{foreach from=$oxajax.container1 item=aItem key=iKey}]
            [{$sSep}][{strip}]{ key:'_[{$iKey}]', ident: [{if $aItem.4}]true[{else}]false[{/if}]
                [{if !$aItem.4}],
                label: '[{oxmultilang ident="GENERAL_AJAX_SORT_"|cat:$aItem.0|oxupper}]',
                visible: [{if $aItem.2}]true[{else}]false[{/if}]
                [{/if}]}
                [{/strip}]
                [{assign var="sSep" value=","}]
            [{/foreach}] ],
            '[{$oViewConf->getAjaxLink()}]cmpid=container1&container=sliderelementcategory&synchoxid=[{$oxid}]'
        );

        [{assign var="sSep" value=""}]

        YAHOO.oxid.container2 = new YAHOO.oxid.aoc('container2',
            [ [{foreach from=$oxajax.container2 item=aItem key=iKey}]
            [{$sSep}][{strip}]{ key:'_[{$iKey}]', ident: [{if $aItem.4}]true[{else}]false[{/if}]
                [{if !$aItem.4}],
                label: '[{oxmultilang ident="GENERAL_AJAX_SORT_"|cat:$aItem.0|oxupper}]',
                visible: [{if $aItem.2}]true[{else}]false[{/if}]
                [{/if}]}
                [{/strip}]
                [{assign var="sSep" value=","}]
            [{/foreach}] ],
            '[{$oViewConf->getAjaxLink()}]cmpid=container2&container=sliderelementcategory&oxid=[{$oxid}]'
        );

        YAHOO.oxid.container1.getDropAction = function()
        {
            return 'fnc=addCategoryToSliderElement';
        }

        YAHOO.oxid.container2.getDropAction = function()
        {
            return 'fnc=removeCategoryFromSliderElement';
        }

        YAHOO.oxid.container2.subscribe("rowClickEvent", function(oParam)
        {
            var aSelRows= YAHOO.oxid.container2.getSelectedRows();
            if (aSelRows.length)
            {
                oParam = YAHOO.oxid.container2.getRecord(aSelRows[0]);
            }
        });

        YAHOO.oxid.container2.subscribe("dataReturnEvent", function()
        {
        });

        YAHOO.oxid.container2.onSave = function()
        {
            YAHOO.oxid.container1.getDataSource().flushCache();
            YAHOO.oxid.container1.getPage(0);
            YAHOO.oxid.container2.getDataSource().flushCache();
            YAHOO.oxid.container2.getPage(0);
        }
        YAHOO.oxid.container2.onFailure = function() { /* currently does nothing */ }

        YAHOO.oxid.container1.onSuccessCalback = reloadParentFrame;
        YAHOO.oxid.container2.onSuccessCalback = reloadParentFrame;
    }
    $E.onDOMReady(initAoc);

    // reloading frame after assignment..
    function reloadParentFrame()
    {
        try
        {
            if (window.opener && window.opener.document && window.opener.document.myedit)
            {
                window.opener.document.myedit.submit();
            }
        } catch (oErr) {}
    }
    -->
</script>

<table width="100%">
    <colgroup>
        <col span="2" width="50%" />
    </colgroup>
    <tr class="edittext">
        <td colspan="3">[{oxmultilang ident="GENERAL_AJAX_DESCRIPTION"}]<br/>[{oxmultilang ident="GENERAL_FILTERING"}]<br /><br /></td>
    </tr>
    <tr class="edittext">
        <td align="center"><b>[{oxmultilang ident="GENERAL_CATEGORYSELECT"}]</b></td>
        <td align="center"><b>[{oxmultilang ident="TRWSLIDER_ASSIGNEDCATEGORIES"}]</b></td>
    </tr>
    <tr>
        <td valign="top" id="container1"></td>
        <td valign="top" id="container2"></td>
    </tr>
    <tr>
        <td class="oxid-aoc-actions"><input type="button" value="[{oxmultilang ident="GENERAL_AJAX_ASSIGNALL"}]" id="container1_btn"></td>
        <td class="oxid-aoc-actions"><input type="button" value="[{oxmultilang ident="GENERAL_AJAX_UNASSIGNALL"}]" id="container2_btn"></td>
    </tr>
</table>
</body>
</html>
