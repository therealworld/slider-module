[{include file="headitem.tpl" title="TRWSLIDER_TEMPLATE_TITLE"|oxmultilangassign}]

<script type="text/javascript">
    <!--
    function TRWSliderInsert()
    {
        [{foreach from=$aElements item=oElement}]
            copyLongDesc('trwsliderelements__oxdesc_[{$oElement->getId()}]');
        [{/foreach}]
        return true;
    }
    //-->
</script>

[{if $readonly}]
    [{assign var="readonly" value="readonly disabled"}]
[{else}]
    [{assign var="readonly" value=""}]
[{/if}]

<form name="transfer" id="transfer" action="[{$oViewConf->getSelfLink()}]" method="post">
    [{$oViewConf->getHiddenSid()}]
    <input type="hidden" name="oxid" value="[{$oxid}]" />
    <input type="hidden" name="cl" value="sliderextend" />
    <input type="hidden" name="editlanguage" value="[{$editlanguage}]" />
</form>

<form name="myedit" id="myedit" action="[{$oViewConf->getSelfLink()}]" method="post" enctype="multipart/form-data" onSubmit="return TRWSliderInsert()">
    [{$oViewConf->getHiddenSid()}]
    <input type="hidden" name="cl" value="sliderextend" />
    <input type="hidden" name="fnc" value="" />
    <input type="hidden" name="oxid" value="[{$oxid}]" />
    <input type="hidden" name="sDeleteElement" id="DeleteEl" value="" />
    <input type="hidden" name="MAX_FILE_SIZE" value="[{$iMaxUploadFileSize}]" />

    <table cellspacing="0" cellpadding="0" border="0" width="98%">
        <tr class="tr-row">
            <td class="edittext">
            <button class="saveButton" type="button" class="edittext" name="save" onclick="addNewElement();" [{$readonly}]>[{oxmultilang ident="TRWSLIDER_ADDNEWELEMENT"}]</button>
            </td>
        </tr>
    </table>

    <table cellspacing="0" cellpadding="0" border="0" width="98%">
        [{foreach from=$aElements item=oElement}]
            [{assign var="sElOxId" value=$oElement->getId()}]
            <tr class="tr-row">
                <td class="edittext">
                    <input type="hidden" class="sort-row" name="sort[]" value="[{$sElOxId}]" />
                    <button class="saveButton" type="button" onclick="moveRow(this, 'sort-row', 'up');" title="[{oxmultilang ident="TRWSLIDER_ELEMENT_UP"}]">&#8593;</button><br />
                    <button class="saveButton" type="button" onclick="moveRow(this, 'sort-row', 'down');" title="[{oxmultilang ident="TRWSLIDER_ELEMENT_DOWN"}]">&#8595;</button>
                </td>
                <td>
                    <table cellspacing="0" cellpadding="0" border="0">
                        [{block name="admin_trwslider_extend_form"}]
                            [{if $oxid != "-1"}]
                                <tr>
                                    <td class="edittext" width="90">
                                        [{oxmultilang ident="GENERAL_ACTIVE"}]
                                    </td>
                                    <td class="edittext">
                                        <input class="edittext" type="hidden" name="editvalarr[[{$sElOxId}]][trwsliderelements__oxactive]" value="0" />
                                        <input class="edittext" type="checkbox" name="editvalarr[[{$sElOxId}]][trwsliderelements__oxactive]" value="1" [{if $oElement->trwsliderelements__oxactive->value == 1}]checked[{/if}] [{$readonly}] />
                                        [{oxinputhelp ident="HELP_GENERAL_ACTIVE"}]
                                    </td>
                                </tr>
                                <tr>
                                    <td class="edittext">
                                        [{oxmultilang ident="TRWSLIDER_FULLWIDTH"}]
                                    </td>
                                    <td class="edittext">
                                        <input class="edittext" type="hidden" name="editvalarr[[{$sElOxId}]][trwsliderelements__oxfullwidth]" value="0" />
                                        <input class="edittext" type="checkbox" name="editvalarr[[{$sElOxId}]][trwsliderelements__oxfullwidth]" value="1" [{if $oElement->trwsliderelements__oxfullwidth->value == 1}]checked[{/if}] [{$readonly}] />
                                        [{oxinputhelp ident="HELP_TRWSLIDER_FULLWIDTH"}]
                                    </td>
                                </tr>
                                [{if $oViewConf->getConfigParam('bTRWSliderIndividualBtn')}]
                                    <tr>
                                        <td class="edittext">
                                            [{oxmultilang ident="TRWSLIDER_BGCOLOR"}]
                                        </td>
                                        <td class="edittext">
                                            <input type="text" class="editinput" size="6" maxlength="[{$oElement->trwsliderelements__oxbgcolor->fldmax_length}]" name="editvalarr[[{$sElOxId}]][trwsliderelements__oxbgcolor]" value="[{$oElement->trwsliderelements__oxbgcolor->value}]" [{$readonly}] />
                                            [{oxinputhelp ident="HELP_TRWSLIDER_BGCOLOR"}]
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="edittext">
                                            [{oxmultilang ident="TRWSLIDER_BTNCOLOR"}]
                                        </td>
                                        <td class="edittext">
                                            <input type="text" class="editinput" size="6" maxlength="[{$oElement->trwsliderelements__oxbtncolor->fldmax_length}]" name="editvalarr[[{$sElOxId}]][trwsliderelements__oxbtncolor]" value="[{$oElement->trwsliderelements__oxbtncolor->value}]" [{$readonly}] />
                                            [{oxinputhelp ident="HELP_TRWSLIDER_BTNCOLOR"}]
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="edittext">
                                            [{oxmultilang ident="TRWSLIDER_BTNBGCOLOR"}]
                                        </td>
                                        <td class="edittext">
                                            <input type="text" class="editinput" size="6" maxlength="[{$oElement->trwsliderelements__oxbtnbgcolor->fldmax_length}]" name="editvalarr[[{$sElOxId}]][trwsliderelements__oxbtnbgcolor]" value="[{$oElement->trwsliderelements__oxbtnbgcolor->value}]" [{$readonly}] />
                                            [{oxinputhelp ident="HELP_TRWSLIDER_BTNBGCOLOR"}]
                                        </td>
                                    </tr>
                                [{/if}]
                                <tr>
                                    <td class="edittext">
                                        [{oxmultilang ident="GENERAL_TITLE"}]
                                    </td>
                                    <td class="edittext">
                                        <input type="text" class="editinput" size="25" maxlength="[{$oElement->trwsliderelements__oxtitle->fldmax_length}]" name="editvalarr[[{$sElOxId}]][trwsliderelements__oxtitle]" value="[{$oElement->trwsliderelements__oxtitle->value}]" [{$readonly}] />
                                        [{oxinputhelp ident="HELP_GENERAL_TITLE"}]
                                    </td>
                                </tr>

                                <tr>
                                    <td class="edittext">
                                        [{oxmultilang ident="GENERAL_DATE"}]
                                    </td>
                                    <td class="edittext">
                                    [{oxmultilang ident="GENERAL_FROM"}]<input type="text" class="editinput" size="25" maxlength="[{$oElement->trwsliderelements__oxdatefrom->fldmax_length}]" name="editvalarr[[{$sElOxId}]][trwsliderelements__oxdatefrom]" value="[{$oElement->trwsliderelements__oxdatefrom|oxformdate}]" [{include file="help.tpl" helpid=article_vonbis}] [{$readonly}] /><br />
                                    [{oxmultilang ident="GENERAL_TILL"}] <input type="text" class="editinput" size="25" maxlength="[{$oElement->trwsliderelements__oxdateto->fldmax_length}]" name="editvalarr[[{$sElOxId}]][trwsliderelements__oxdateto]" value="[{$oElement->trwsliderelements__oxdateto|oxformdate}]" [{include file="help.tpl" helpid=article_vonbis}] [{$readonly}] />
                                    [{oxinputhelp ident="HELP_GENERAL_DATE"}]
                                    </td>
                                </tr>

                                <tr>
                                    <td class="edittext">
                                        [{oxmultilang ident="TRWSLIDER_PICTURETITLE"}]
                                    </td>
                                    <td class="edittext">
                                        <input type="text" class="editinput" size="25" maxlength="[{$oElement->trwsliderelements__oxpictitle->fldmax_length}]" name="editvalarr[[{$sElOxId}]][trwsliderelements__oxpictitle]" value="[{$oElement->trwsliderelements__oxpictitle->value}]" [{$readonly}] />
                                        [{oxinputhelp ident="HELP_TRWSLIDER_PICTURETITLE"}]
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        [{oxmultilang ident="TRWSLIDER_PICTURE"}]
                                    </td>
                                    <td>
                                        [{if (!($oElement->trwsliderelements__oxpic->value=="nopic.jpg" || $oElement->trwsliderelements__oxpic->value==""))}]
                                            <a href="#" onclick="deletePicAndSubmit('oxpic_[{$sElOxId}]');" class="delete left" [{include file="help.tpl" helpid=item_delete}]></a>
                                        [{/if}]
                                        <input id="oxpic_[{$sElOxId}]" type="text" size="25" maxlength="[{$oElement->trwsliderelements__oxpic->fldmax_length}]" name="editvalarr[[{$sElOxId}]][trwsliderelements__oxpic]" value="[{$oElement->trwsliderelements__oxpic->value}]" readonly />
                                        [{oxinputhelp ident="HELP_TRWSLIDER_PICTURE"}]
                                    </td>
                                </tr>

                                <tr>
                                    <td class="edittext">
                                        [{oxmultilang ident="TRWSLIDER_PICTUREUPLOAD"}]<br />
                                        ([{oxmultilang ident="GENERAL_MAX_FILE_UPLOAD"}] [{$sMaxFormattedFileSize}], [{oxmultilang ident="TRWSLIDER_PICTURE_DIMENSIONS" args=$sTRWSliderPicSize}])
                                    </td>
                                    <td class="edittext">
                                        <input onclick="deletePic('oxpic_[{$sElOxId}]');" class="editinput" name="myfile_[{$sElOxId}][TRWSLIDER@trwsliderelements__oxpic]" type="file" size="26" [{$readonly}] />
                                        [{oxinputhelp ident="HELP_TRWSLIDER_PICTUREUPLOAD"}]
                                    </td>
                                </tr>
                                [{if $oElement->trwsliderelements__oxpic->value}]
                                    <tr>
                                        <td></td>
                                        <td>
                                            <img src="[{$oElement->getSliderPictureUrl()}]" alt="[{oxmultilang ident="TRWSLIDER_PICTURE"}]" width="120" />
                                        </td>
                                    </tr>
                                [{/if}]

                                <tr>
                                    <td>
                                        [{oxmultilang ident="TRWSLIDER_PICTUREALTERNATIVE"}]
                                    </td>
                                    <td>
                                        [{if (!($oElement->trwsliderelements__oxpicalternative->value=="nopic.jpg" || $oElement->trwsliderelements__oxpicalternative->value==""))}]
                                            <a href="#" onclick="deletePicAndSubmit('oxpicalternative_[{$sElOxId}]');" class="delete left" [{include file="help.tpl" helpid=item_delete}]></a>
                                        [{/if}]
                                        <input id="oxpicalternative_[{$sElOxId}]" type="text" size="25" maxlength="[{$oElement->trwsliderelements__oxpicalternative->fldmax_length}]" name="editvalarr[[{$sElOxId}]][trwsliderelements__oxpicalternative]" value="[{$oElement->trwsliderelements__oxpicalternative->value}]" readonly />
                                        [{oxinputhelp ident="HELP_TRWSLIDER_PICTURE"}]
                                    </td>
                                </tr>

                                <tr>
                                    <td class="edittext">
                                        [{oxmultilang ident="TRWSLIDER_PICTUREALTERNATIVEUPLOAD"}]<br />
                                        ([{oxmultilang ident="GENERAL_MAX_FILE_UPLOAD"}] [{$sMaxFormattedFileSize}], [{oxmultilang ident="TRWSLIDER_PICTURE_DIMENSIONS" args=$sTRWSliderPicAlternativeSize}])
                                    </td>
                                    <td class="edittext">
                                        <input onclick="deletePic('oxpicalternative_[{$sElOxId}]');" class="editinput" name="myfile_[{$sElOxId}][TRWSLIDERALTERNATIVE@trwsliderelements__oxpicalternative]" type="file" size="26" [{$readonly}] />
                                        [{oxinputhelp ident="HELP_TRWSLIDER_PICTUREALTERNATIVE"}]
                                    </td>
                                </tr>
                                [{if $oElement->trwsliderelements__oxpicalternative->value}]
                                    <tr>
                                        <td></td>
                                        <td>
                                            <img src="[{$oElement->getSliderPictureAlternativeUrl()}]" alt="[{oxmultilang ident="TRWSLIDER_PICTUREALTERNATIVE"}]" width="120" />
                                        </td>
                                    </tr>
                                [{/if}]


                                <tr>
                                    <td class="edittext" colspan="2">
                                        <br /><br /><br />
                                        <button class="saveButton" type="button" onclick="submitForm();" [{$readonly}]>[{oxmultilang ident="GENERAL_SAVE"}]</button>
                                        <button class="saveButton" type="button" onclick="addNewElement();" [{$readonly}]>[{oxmultilang ident="TRWSLIDER_ADDNEWELEMENT"}]</button>
                                        <button class="saveButton" type="button" onclick="deleteElement('[{$sElOxId}]');" [{$readonly}]>[{oxmultilang ident="TRWSLIDER_DELETEELEMENT"}]</button>
                                    </td>
                                </tr>
                            [{/if}]
                        [{/block}]
                    </table>
                </td>

                <td width="50%">
                    <table cellspacing="0" cellpadding="0" border="0">
                        [{block name="admin_trwslider_extend_connect"}]
                            [{if $oxid != "-1"}]
                                <tr>
                                    <td class="edittext">
                                        <h3>[{oxmultilang ident="TRWSLIDER_LINK"}]</h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="edittext" width="90">
                                        <b>[{oxmultilang ident="TRWSLIDER_LINKACTUAL"}]</b>
                                    </td>
                                    <td width="90%">
                                        [{if $oElement->getLink()}]<a href="[{$oElement->getLink()}]" target="_blank">[{$oElement->getLink()}]</a>[{else}]-[{/if}]
                                    </td>
                                </tr>
                                <tr>
                                    <td class="edittext">
                                        [{oxmultilang ident="GENERAL_LINK"}]
                                    </td>
                                    <td class="edittext">
                                        <input type="text" class="editinput" size="25" maxlength="[{$oElement->trwsliderelements__oxlink->fldmax_length}]" name="editvalarr[[{$sElOxId}]][trwsliderelements__oxlink]" value="[{$oElement->trwsliderelements__oxlink->value}]" [{$readonly}] />
                                        [{oxinputhelp ident="HELP_GENERAL_LINK"}]
                                    </td>
                                </tr>
                                <tr>
                                    <td class="edittext">
                                        [{oxmultilang ident="GENERAL_OR"}]
                                    </td>
                                    <td class="edittext">
                                        <input [{$readonly}] type="button" value="[{oxmultilang ident="GENERAL_ASSIGNCATEGORIES"}]" class="edittext" onclick="showDialog('&cl=SliderExtend&aoc=2&oxid=[{$sElOxId}]');" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="edittext">
                                        [{oxmultilang ident="GENERAL_OR"}]
                                    </td>
                                    <td class="edittext">
                                        <input [{$readonly}] type="button" value="[{oxmultilang ident="GENERAL_ASSIGNARTICLE"}]" class="edittext" onclick="showDialog('&cl=SliderExtend&aoc=1&oxid=[{$sElOxId}]');" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="edittext">
                                        [{oxmultilang ident="TRWSLIDER_LINKTARGET"}]
                                    </td>
                                    <td class="edittext">
                                        <input type="text" class="editinput" size="6" maxlength="[{$oElement->trwsliderelements__oxlinktarget->fldmax_length}]" name="editvalarr[[{$sElOxId}]][trwsliderelements__oxlinktarget]" value="[{$oElement->trwsliderelements__oxlinktarget->value}]" [{$readonly}] />
                                        [{oxinputhelp ident="HELP_TRWSLIDER_LINKTARGET"}]
                                    </td>
                                </tr>
                                <tr>
                                    <td class="edittext">
                                        [{oxmultilang ident="GENERAL_DESCRIPTION"}] [{oxinputhelp ident="HELP_GENERAL_DESCRIPTION"}]
                                    </td>
                                    <td class="edittext">
                                        [{if !$oViewConf->getConfigParam('bTRWSliderUseWYSIWYG')}]
                                            <textarea cols="36" style="height: 200px; width: 100%;" name="editvalarr[[{$sElOxId}]][trwsliderelements__oxdesc]" [{$readonly}]>[{$oElement->trwsliderelements__oxdesc->value}]</textarea>
                                        [{else}]
                                            <input type="hidden" name="editval[trwsliderelements__oxdesc_[{$sElOxId}]]" value="" />
                                            [{$aEditors.$sElOxId}]
                                        [{/if}]
                                    </td>
                                </tr>
                            [{/if}]
                        [{/block}]
                    </table>
                </td>
            </tr>
        [{/foreach}]
    </table>

        [{if $aElements}]
            <table cellspacing="0" cellpadding="0" border="0" width="98%">
                <tr>
                    <td class="edittext">
                        [{include file="language_edit.tpl"}]
                    </td>
                </tr>
                <tr>
                    <td class="edittext">
                        <button class="saveButton" type="button" class="edittext" name="save" onclick="submitForm();" [{$readonly}] />[{oxmultilang ident="GENERAL_SAVE"}]</button>
                    </td>
                </tr>
            </table>
        [{/if}]

</form>
[{include file="bottomnaviitem.tpl"}]
[{include file="bottomitem.tpl"}]