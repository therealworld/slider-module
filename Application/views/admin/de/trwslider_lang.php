<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset' => 'UTF-8',

    'mxtrwslider'             => 'Slider verwalten',
    'tbclslider_slidermain'   => 'Stamm',
    'tbclslider_sliderextend' => 'Erweitert',
    'TOOLTIPS_NEWSLIDER'      => 'Neuen Slider anlegen',
    'TOOLTIPS_NEWPROMOTION'   => 'Neue Aktion',

    'GENERAL_ASSIGNCONTENT'        => 'CMS-Inhalte zuordnen',
    'TRWSLIDER_ASSIGN'             => 'Slider zu Webseiten zuordnen',
    'TRWSLIDER_ASSIGNSTARTPAGE'    => 'Slider zur Startseite zuordnen',
    'TRWSLIDER_ASSIGNFOOTER'       => 'Slider zum Fussbereich zuordnen',
    'TRWSLIDER_ASSIGNEDARTICLE'    => 'zugeordnete Produkte',
    'TRWSLIDER_ASSIGNEDCATEGORIES' => 'zugeordnete Kategorien',
    'TRWSLIDER_ASSIGNEDCONTENTS'   => 'zugeordnete Inhalte',
    'TRWSLIDER_STARTPAGE'          => 'Startseite?',
    'TRWSLIDER_FOOTER'             => 'Footer?',
    'TRWSLIDER_LINK'               => 'Link',
    'TRWSLIDER_LINKTARGET'         => 'Linkziel',
    'TRWSLIDER_LINKACTUAL'         => 'aktueller Link',

    'TRWSLIDER_SELECTONEARTICLE'         => 'Bitte wählen Sie einen Artikel',
    'TRWSLIDER_SELECTONECATEGORY'        => 'Bitte wählen Sie einen Kategorie',
    'TRWSLIDER_SELECTONECONTENT'         => 'Bitte wählen Sie einen Inhalt',
    'TRWSLIDER_PICTURE'                  => 'Bild',
    'TRWSLIDER_PICTUREUPLOAD'            => 'Bild auswählen',
    'TRWSLIDER_PICTUREALTERNATIVE'       => 'alternatives Bild',
    'TRWSLIDER_PICTUREALTERNATIVEUPLOAD' => 'alternatives Bild auswählen',
    'TRWSLIDER_PICTURETITLE'             => 'Bild Titel',
    'TRWSLIDER_ADDNEWELEMENT'            => 'Neues Slider-Element anlegen',
    'TRWSLIDER_DELETEELEMENT'            => 'Dieses Slider-Element löschen',
    'TRWSLIDER_BGCOLOR'                  => 'Hintergrund-Farbe Slider-Element',
    'TRWSLIDER_BTNCOLOR'                 => 'Farbe Button',
    'TRWSLIDER_BTNBGCOLOR'               => 'Hintergrund-Farbe Button',
    'TRWSLIDER_FULLWIDTH'                => 'Slider-Element in voller Breite zeigen',
    'TRWSLIDER_ELEMENT_UP'               => 'Slider-Element Position nach vorn',
    'TRWSLIDER_ELEMENT_DOWN'             => 'Slider-Element Position nach hinten',

    'TRWSLIDER_TEMPLATE_TITLE' => '[the-real-world Slider]',

    'TRWSLIDER_OXPOS'                       => 'Position',
    'TRWSLIDER_OXPOS_PAGEHEADER'            => 'Seiten Kopf',
    'TRWSLIDER_OXPOS_PAGEHEADERALTERNATIVE' => 'Seiten Kopf (alternative Gestaltung)',
    'TRWSLIDER_OXPOS_MAINHEADER'            => 'Inhaltebereich Kopf',
    'TRWSLIDER_OXPOS_MAINFOOTER'            => 'Inhaltebereich Fuss',

    'TRWSLIDER_PICTURE_DIMENSIONS' => 'max. %s px',

    'SHOP_MALL_MALLINHERIT_TRWSLIDER' => 'Alle <b>(TRW) Slider</b> vom Elternshop erben',
];
