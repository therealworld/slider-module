<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset' => 'UTF-8',

    'SHOP_MODULE_GROUP_trwslidercontroller' => 'Slider für Seitentyp ... aktivieren',
    'SHOP_MODULE_GROUP_trwsliderposition'   => 'Anzeigepositionen im Backend anzeigen',
    'SHOP_MODULE_GROUP_trwsliderdefault'    => 'Slider Standard-Position je Seitentyp',
    'SHOP_MODULE_GROUP_trwslideroptions'    => 'Slider Optionen',

    'SHOP_MODULE_bTRWSliderShowCtrlStart'      => 'Startseite',
    'SHOP_MODULE_bTRWSliderShowCtrlArticle'    => 'Produkt-Detailseiten',
    'SHOP_MODULE_bTRWSliderShowCtrlCategory'   => 'Kategorien',
    'SHOP_MODULE_bTRWSliderShowCtrlContent'    => 'CMS-Seiten',
    'SHOP_MODULE_bTRWSliderShowCtrlFooter'     => 'Fussbereich',
    'SHOP_MODULE_sTRWSliderDefaultStart'       => 'Startseite',
    'SHOP_MODULE_sTRWSliderDefaultArticle'     => 'Produkt-Detailseiten',
    'SHOP_MODULE_sTRWSliderDefaultCategory'    => 'Kategorien',
    'SHOP_MODULE_sTRWSliderDefaultContent'     => 'CMS-Seiten',
    'SHOP_MODULE_bTRWSliderUseWYSIWYG'         => 'Nutze den WYSIWYG-Editor (wenn vorhanden) für die Bearbeitung der Slider-Beschreibung',
    'SHOP_MODULE_bTRWSliderIndividualBtn'      => 'Slider-Hintergründe & -Buttons farblich anpassen',
    'SHOP_MODULE_sTRWSliderPicSize'            => 'Format des Slider-Bildes (z.B. 1000*400 - Breite*Höhe)',
    'SHOP_MODULE_sTRWSliderPicAlternativeSize' => 'Format des alternativen Slider-Bildes (z.B. 500*400 - Breite*Höhe)',
];

$aLangOption = [
    'mainheader'            => 'Kopf Inhaltebereich',
    'pageheader'            => 'Kopf Webseite',
    'pageheaderalternative' => 'Kopf Webseite (alternative Gestaltung)',
    'mainfooter'            => 'Fuss Inhaltebereich',
];

$aLangOptionWrapper = [
    'SHOP_MODULE_bTRWSliderPos',
    'SHOP_MODULE_sTRWSliderDefaultStart_',
    'SHOP_MODULE_sTRWSliderDefaultArticle_',
    'SHOP_MODULE_sTRWSliderDefaultCategory_',
    'SHOP_MODULE_sTRWSliderDefaultContent_',
];

foreach ($aLangOptionWrapper as $sLangOptionWrapper) {
    foreach ($aLangOption as $sLangOptionKey => $sLangOption) {
        $aLang[$sLangOptionWrapper . $sLangOptionKey] = $sLangOption;
    }
}
