<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset' => 'UTF-8',

    'SHOP_MODULE_GROUP_trwslidercontroller' => 'Activate slider for page type ...',
    'SHOP_MODULE_GROUP_trwsliderposition'   => 'Display Slider positions in the backend',
    'SHOP_MODULE_GROUP_trwsliderdefault'    => 'Slider Default position per page type',
    'SHOP_MODULE_GROUP_trwslideroptions'    => 'Slider Options',

    'SHOP_MODULE_bTRWSliderShowCtrlStart'      => 'Startpage',
    'SHOP_MODULE_bTRWSliderShowCtrlArticle'    => 'Product-Detailpages',
    'SHOP_MODULE_bTRWSliderShowCtrlCategory'   => 'Categories',
    'SHOP_MODULE_bTRWSliderShowCtrlContent'    => 'CMS-Pages',
    'SHOP_MODULE_bTRWSliderShowCtrlFooter'     => 'Footer',
    'SHOP_MODULE_sTRWSliderDefaultStart'       => 'Startpage',
    'SHOP_MODULE_sTRWSliderDefaultArticle'     => 'Product-Detailpages',
    'SHOP_MODULE_sTRWSliderDefaultCategory'    => 'Categories',
    'SHOP_MODULE_sTRWSliderDefaultContent'     => 'CMS-Pages',
    'SHOP_MODULE_bTRWSliderUseWYSIWYG'         => 'Use WYSIWYG-Editor for edit the sliderdescription',
    'SHOP_MODULE_bTRWSliderIndividualBtn'      => 'Individual slider-backgrounds & -buttons',
    'SHOP_MODULE_sTRWSliderPicSize'            => 'Format of Slider Picture (e.g. 1000*400 - Width*Height)',
    'SHOP_MODULE_sTRWSliderPicAlternativeSize' => 'Format of alternative Slider Picture (e.g. 500*400 - Width*Height)',
];

$aLangOption = [
    'mainheader'            => 'Header Maincontent',
    'pageheader'            => 'Header Page',
    'pageheaderalternative' => 'Header Page (alternative Version)',
    'mainfooter'            => 'Footer Maincontent',
];

$aLangOptionWrapper = [
    'SHOP_MODULE_bTRWSliderPos',
    'SHOP_MODULE_sTRWSliderDefaultStart_',
    'SHOP_MODULE_sTRWSliderDefaultArticle_',
    'SHOP_MODULE_sTRWSliderDefaultCategory_',
    'SHOP_MODULE_sTRWSliderDefaultContent_',
];

foreach ($aLangOptionWrapper as $sLangOptionWrapper) {
    foreach ($aLangOption as $sLangOptionKey => $sLangOption) {
        $aLang[$sLangOptionWrapper . $sLangOptionKey] = $sLangOption;
    }
}
