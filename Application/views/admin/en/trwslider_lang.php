<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset' => 'UTF-8',

    'mxtrwslider'             => 'Slider Settings',
    'tbclslider_slidermain'   => 'Main',
    'tbclslider_sliderextend' => 'Extend',
    'TOOLTIPS_NEWSLIDER'      => 'Create New Slider',
    'TOOLTIPS_NEWPROMOTION'   => 'New Action',

    'GENERAL_ASSIGNCONTENT'        => 'Assign Content',
    'TRWSLIDER_ASSIGN'             => 'Assign Slider on Webpage',
    'TRWSLIDER_ASSIGNSTARTPAGE'    => 'Assign Slider to Startpage',
    'TRWSLIDER_ASSIGNFOOTER'       => 'Assign Slider to Footer',
    'TRWSLIDER_ASSIGNEDARTICLE'    => 'Assigned Articles',
    'TRWSLIDER_ASSIGNEDCATEGORIES' => 'Assigned Categories',
    'TRWSLIDER_ASSIGNEDCONTENTS'   => 'Assigned Contents',
    'TRWSLIDER_STARTPAGE'          => 'Startpage?',
    'TRWSLIDER_FOOTER'             => 'Footer?',
    'TRWSLIDER_LINK'               => 'Link',
    'TRWSLIDER_LINKTARGET'         => 'Linktarget',
    'TRWSLIDER_LINKACTUAL'         => 'Actual Link',

    'TRWSLIDER_SELECTONEARTICLE'         => 'please select an Article',
    'TRWSLIDER_SELECTONECATEGORY'        => 'please select a Category',
    'TRWSLIDER_SELECTONECONTENT'         => 'please select an Content',
    'TRWSLIDER_PICTURE'                  => 'picture',
    'TRWSLIDER_PICTUREUPLOAD'            => 'Choose picture',
    'TRWSLIDER_PICTUREALTERNATIVE'       => 'alternative picture',
    'TRWSLIDER_PICTUREALTERNATIVEUPLOAD' => 'Choose alternative picture',
    'TRWSLIDER_PICTURETITLE'             => 'Picture Title',
    'TRWSLIDER_ADDNEWELEMENT'            => 'Add new Slider-Element',
    'TRWSLIDER_DELETEELEMENT'            => 'delete this Slider-Element',
    'TRWSLIDER_BGCOLOR'                  => 'Background-Color of Slide-Element',
    'TRWSLIDER_BTNCOLOR'                 => 'Color of Button',
    'TRWSLIDER_BTNBGCOLOR'               => 'Background-Color of Button',
    'TRWSLIDER_FULLWIDTH'                => 'Slider-Element in full width',
    'TRWSLIDER_ELEMENT_UP'               => 'Slider-Element Position up',
    'TRWSLIDER_ELEMENT_DOWN'             => 'Slider-Element Position down',

    'TRWSLIDER_TEMPLATE_TITLE' => '[the-real-world Slider]',

    'TRWSLIDER_OXPOS'                       => 'Position',
    'TRWSLIDER_OXPOS_PAGEHEADER'            => 'Page Header',
    'TRWSLIDER_OXPOS_PAGEHEADERALTERNATIVE' => 'Page Header (alternative Layout)',
    'TRWSLIDER_OXPOS_MAINHEADER'            => 'Main Header',
    'TRWSLIDER_OXPOS_MAINFOOTER'            => 'Main Footer',

    'TRWSLIDER_PICTURE_DIMENSIONS' => 'max. %s px',

    'SHOP_MALL_MALLINHERIT_TRWSLIDER' => 'Inherit all <b>(TRW) Slider</b> from parent shop',
];
