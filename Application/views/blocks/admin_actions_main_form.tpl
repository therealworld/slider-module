<tr>
    <td class="edittext" width="120">
        [{oxmultilang ident="GENERAL_NAME"}]
    </td>
    <td class="edittext">
        <input type="hidden" name="editval[oxactions__oxtype]" value="1" />
        <input type="text" class="editinput" size="32" maxlength="[{$edit->oxactions__oxtitle->fldmax_length}]" name="editval[oxactions__oxtitle]" value="[{$edit->oxactions__oxtitle->value}]" [{$readonly}] />
        [{oxinputhelp ident="HELP_GENERAL_NAME"}]
    </td>
</tr>

<tr>
    <td class="edittext" width="120">
        [{oxmultilang ident="GENERAL_ALWAYS_ACTIVE"}]
    </td>
    <td class="edittext">
        <input class="edittext" type="checkbox" name="editval[oxactions__oxactive]" value='1' [{if $edit->oxactions__oxactive->value == 1}]checked[{/if}] [{$readonly}] />
        [{oxinputhelp ident="HELP_GENERAL_ACTIVE"}]
    </td>
</tr>
<tr>
    <td class="edittext">
        [{oxmultilang ident="GENERAL_ACTIVFROMTILL"}]
    </td>
    <td class="edittext" align="right">
        [{oxmultilang ident="GENERAL_FROM"}] <input type="text" class="editinput" size="27" name="editval[oxactions__oxactivefrom]" value="[{$edit->oxactions__oxactivefrom|oxformdate}]" [{include file="help.tpl" helpid=article_vonbis}] [{$readonly}] /><br />
        [{oxmultilang ident="GENERAL_TILL"}] <input type="text" class="editinput" size="27" name="editval[oxactions__oxactiveto]" value="[{$edit->oxactions__oxactiveto|oxformdate}]" [{include file="help.tpl" helpid=article_vonbis}] [{$readonly}] />
        [{oxinputhelp ident="HELP_GENERAL_ACTIVFROMTILL"}]
    </td>
</tr>
