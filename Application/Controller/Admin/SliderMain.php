<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SliderModule\Application\Controller\Admin;

use Exception;
use OxidEsales\Eshop\Application\Controller\Admin\AdminDetailsController;
use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
use OxidEsales\Eshop\Core\Exception\DatabaseErrorException;
use OxidEsales\Eshop\Core\Registry;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use TheRealWorld\SliderModule\Application\Model\Slider;
use TheRealWorld\SliderModule\Application\Model\Slider2Object;
use TheRealWorld\SliderModule\Core\SliderHelper;
use TheRealWorld\ToolsPlugin\Core\ToolsDB;

/**
 * provides a Slider-Configuration
 * Admin Menu: Customer Information -> Slider Settings.
 */
class SliderMain extends AdminDetailsController
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_sThisTemplate = 'slidermain.tpl';

    /**
     * OXID-Core.
     * {@inheritDoc}
     *
     * @throws DatabaseErrorException
     * @throws NotFoundExceptionInterface
     * @throws ContainerExceptionInterface
     */
    public function render()
    {
        parent::render();

        $sOxId = $this->getEditObjectId();
        $this->_aViewData['oxid'] = $sOxId;

        if ($sOxId !== '-1') {
            // load object
            $oTRWSlider = oxNew(Slider::class);
            $oTRWSlider->load($sOxId);

            // Disable editing for derived items
            if ($oTRWSlider->isDerived()) {
                $this->_aViewData['readonly'] = true;
            }

            $this->_aViewData['edit'] = $oTRWSlider;

            $iAoc = Registry::getRequest()->getRequestParameter('aoc');

            if ($iAoc) {
                switch ($iAoc) {
                    // Article
                    case '1':
                        $oSliderAjax = oxNew(SliderArticleAjax::class);
                        $this->_aViewData['oxajax'] = $oSliderAjax->getColumns();
                        $this->_sThisTemplate = 'popupsliderarticle.tpl';

                        break;

                        // Categories
                    case '2':
                        $oSliderAjax = oxNew(SliderCategoryAjax::class);
                        $this->_aViewData['oxajax'] = $oSliderAjax->getColumns();
                        $this->_sThisTemplate = 'popupslidercategory.tpl';

                        break;

                        // Content
                    case '3':
                    default:
                        $oSliderAjax = oxNew(SliderContentAjax::class);
                        $this->_aViewData['oxajax'] = $oSliderAjax->getColumns();
                        $this->_sThisTemplate = 'popupslidercontent.tpl';

                        break;
                }
            } else {
                // Slider Position
                if (
                    ToolsDB::getAnyId(
                        'trwslider2object',
                        [
                            'oxclass'       => 'Start',
                            'oxtrwsliderid' => $sOxId,
                        ]
                    )
                ) {
                    $this->_aViewData['bOnStartPage'] = true;
                    $this->_aViewData['oCtrlStartPosValues'] = SliderHelper::getPosValues($sOxId, 'Start');
                }
                if (
                    ToolsDB::getAnyId(
                        'trwslider2object',
                        [
                            'oxclass'       => 'Footer',
                            'oxtrwsliderid' => $sOxId,
                        ]
                    )
                ) {
                    $this->_aViewData['bOnFooter'] = true;
                }
            }
        }
        $this->_aViewData['sCtrlStartPosDefault'] = SliderHelper::getDefaultPos('Start');

        return $this->_sThisTemplate;
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     *
     * @throws DatabaseConnectionException
     * @throws Exception
     */
    public function save()
    {
        parent::save();

        $sOxId = $this->getEditObjectId();
        $oRequest = Registry::getRequest();
        $aParams = $oRequest->getRequestParameter('editval');

        // checkbox handling
        if (!isset($aParams['trwslider__oxactive'])) {
            $aParams['trwslider__oxactive'] = 0;
        }

        $oTRWSlider = oxNew(Slider::class);

        if ($sOxId === '-1') {
            $aParams['trwslider__oxid'] = null;
        }

        $oTRWSlider->assign($aParams);
        $oTRWSlider->save();

        // set the static slider
        $sStaticPosition = $oRequest->getRequestParameter('staticposition');
        $sStartPagePos = $sStaticPosition === 'Start' ? $oRequest->getRequestParameter('sstartpagepos', '') : '';

        // static Slider Params
        $aParams = [
            'oxclass'       => $sStaticPosition,
            'oxtrwsliderid' => $oTRWSlider->getId(),
            'oxobjectid'    => '',
        ];
        $sSlider2ObjOxId = ToolsDB::getAnyId('trwslider2object', $aParams);

        // set startpage-connection, is new
        if ($sStaticPosition) {
            $oTRWSlider2Object = oxNew(Slider2Object::class);
            if ($sSlider2ObjOxId) {
                $oTRWSlider2Object->load($sSlider2ObjOxId);
            }

            $aParams['oxpos'] = $sStartPagePos;

            $aParams = ToolsDB::convertDB2OxParams($aParams, 'trwslider2object');
            $oTRWSlider2Object->assign($aParams);
            $oTRWSlider2Object->save();
            $oTRWSlider2Object->cleanConnections($sStaticPosition, $oTRWSlider->getId(), $sStartPagePos);
        }

        // delete static-connection, is old
        if (!$sStaticPosition && $sSlider2ObjOxId) {
            $oTRWSlider2Object = oxNew(Slider2Object::class);
            $oTRWSlider2Object->delete($sSlider2ObjOxId);
        }

        // set oxid if inserted
        $this->setEditObjectId($oTRWSlider->getId());
    }
}
