<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SliderModule\Application\Controller\Admin;

use OxidEsales\Eshop\Application\Controller\Admin\AdminListController;

/**
 * provides a Slider-Configuration
 * Admin Menu: Customer Information -> Slider Settings.
 */
class SliderList extends AdminListController
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_sThisTemplate = 'sliderlist.tpl';

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_sListClass = '\TheRealWorld\SliderModule\Application\Model\Slider';

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_sDefSortField = 'oxtitle';
}
