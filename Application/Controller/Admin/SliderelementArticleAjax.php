<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SliderModule\Application\Controller\Admin;

use Exception;
use OxidEsales\Eshop\Application\Controller\Admin\ListComponentAjax;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
use OxidEsales\Eshop\Core\Registry;
use TheRealWorld\SliderModule\Application\Model\SliderElement2Object;
use TheRealWorld\ToolsPlugin\Core\ToolsDB;

/**
 * Class manages slider assignment to articles.
 */
class SliderelementArticleAjax extends ListComponentAjax
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_aColumns = [
        'container1' => [ // field , table,  visible, multilanguage, ident
            ['oxartnum', 'oxarticles', 1, 0, 0],
            ['oxtitle', 'oxarticles', 1, 1, 0],
            ['oxid', 'oxarticles', 0, 0, 1],
        ],
        'container2' => [
            ['oxartnum', 'oxarticles', 1, 0, 0],
            ['oxtitle', 'oxarticles', 1, 1, 0],
            ['oxid', 'oxarticles', 0, 0, 1],
            ['oxid', 'trwsliderelements2object', 0, 0, 1],
        ],
    ];

    /**
     * Removes article from selected sliderelement.
     *
     * @throws DatabaseConnectionException
     */
    public function removeArticleFromSliderElement(): void
    {
        $oRequest = Registry::getRequest();

        $sViewName = 'trwsliderelements2object';

        $aRemoveArticles = $this->_getActionIds($sViewName . '.oxid');
        if ($oRequest->getRequestParameter('all')) {
            $sQ = $this->_addFilter("delete {$sViewName}.* " . $this->_getQuery());
            ToolsDB::execute($sQ);
        } elseif ($aRemoveArticles && is_array($aRemoveArticles)) {
            $sQ = "delete from {$sViewName}
                where {$sViewName}.oxid in (" .
                implode(
                    ', ',
                    DatabaseProvider::getDb()->quoteArray($aRemoveArticles)
                ) .
                ') ';
            ToolsDB::execute($sQ);
        }
    }

    /**
     * Adds Article to selected sliderelement.
     *
     * @throws Exception
     */
    public function addArticleToSliderElement(): void
    {
        $oRequest = Registry::getRequest();

        $sArticleTable = 'oxarticles';

        $aAddArticles = $this->_getActionIds($sArticleTable . '.oxid');

        $sSyncSliderElementOxId = $oRequest->getRequestParameter('synchoxid');
        if ($oRequest->getRequestParameter('all')) {
            $sArticleTable = $this->_getViewName($sArticleTable);
            $aAddArticles = $this->_getAll($this->_addFilter("select {$sArticleTable}.oxid " . $this->_getQuery()));
        }

        if ($sSyncSliderElementOxId && $sSyncSliderElementOxId !== '-1' && is_array($aAddArticles)) {
            $bFirst = true;
            foreach ($aAddArticles as $sAddArticle) {
                // thereby we allow only one mapping
                if ($bFirst) {
                    $bFirst = false;
                    $oTRWSliderElement2Object = oxNew(SliderElement2Object::class);
                    $oTRWSliderElement2Object->cleanElement2Object($sSyncSliderElementOxId, $sAddArticle);

                    $sOxId = ToolsDB::getAnyId(
                        'trwsliderelements2object',
                        [
                            'oxtrwsliderelementid' => $sSyncSliderElementOxId,
                            'oxobjectid'           => $sAddArticle,
                            'oxclass'              => 'Article',
                        ]
                    );
                    if (!$sOxId) {
                        $oTRWSliderElement2Object = oxNew(SliderElement2Object::class);
                        $aParams = [
                            'oxtrwsliderelementid' => $sSyncSliderElementOxId,
                            'oxobjectid'           => $sAddArticle,
                            'oxclass'              => 'Article',
                        ];
                        $aParams = ToolsDB::convertDB2OxParams($aParams, 'trwsliderelements2object');
                        $oTRWSliderElement2Object->assign($aParams);
                        $oTRWSliderElement2Object->save();
                    }
                }
            }
        }
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     *
     * @throws DatabaseConnectionException
     */
    protected function _getQuery()
    {
        $oDb = DatabaseProvider::getDb();
        $oRequest = Registry::getRequest();
        $oConfig = Registry::getConfig();

        // looking for table/view
        $sArticleTable = $this->_getViewName('oxarticles');
        $sViewName = $this->_getViewName('trwsliderelements2object');

        $sSliderElementOxId = $oRequest->getRequestParameter('oxid');
        $sSyncSliderElementOxId = $oRequest->getRequestParameter('synchoxid');

        // Article selected or not?
        if (!$sSliderElementOxId) {
            $sQAdd = " from {$sArticleTable} where 1 ";
        } else {
            $sQAdd = " from {$sArticleTable} left join {$sViewName} on {$sViewName}.oxobjectid = {$sArticleTable}.oxid
                where {$sViewName}.oxtrwsliderelementid = " . $oDb->quote($sSliderElementOxId) . "
                and {$sViewName}.oxclass = 'Article' ";
        }

        $sQAdd .= " and {$sArticleTable}.oxactive = 1 " .
            ($oConfig->getConfigParam('blVariantsSelection') ? '' : " and {$sArticleTable}.oxparentid = '' ");

        if ($sSyncSliderElementOxId && $sSyncSliderElementOxId !== $sSliderElementOxId) {
            $sQAdd .= " and {$sArticleTable}.oxid not in (
                    select {$sArticleTable}.oxid
                    from {$sArticleTable}
                    left join {$sViewName} on {$sViewName}.oxobjectid = {$sArticleTable}.oxid
                    where {$sViewName}.oxtrwsliderelementid = " . $oDb->quote($sSyncSliderElementOxId) . "
                    and {$sViewName}.oxclass = 'Article'
                ) ";
        }

        return $sQAdd;
    }
}
