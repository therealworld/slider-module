<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SliderModule\Application\Controller\Admin;

use Exception;
use OxidEsales\Eshop\Application\Controller\Admin\ListComponentAjax;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
use OxidEsales\Eshop\Core\Exception\DatabaseErrorException;
use OxidEsales\Eshop\Core\Field;
use OxidEsales\Eshop\Core\Registry;
use TheRealWorld\SliderModule\Application\Model\Slider2Object;
use TheRealWorld\SliderModule\Core\SliderHelper;
use TheRealWorld\ToolsPlugin\Core\ToolsDB;

/**
 * Class manages slider assignment to categories.
 */
class SliderCategoryAjax extends ListComponentAjax
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_aColumns = [
        'container1' => [ // field , table,  visible, multilanguage, ident
            ['oxtitle', 'oxcategories', 1, 1, 0],
            ['oxid', 'oxcategories', 0, 0, 1],
        ],
        'container2' => [
            ['oxtitle', 'oxcategories', 1, 1, 0],
            ['oxid', 'oxcategories', 0, 0, 1],
            ['oxid', 'trwslider2object', 0, 0, 1],
            ['oxpos', 'trwslider2object', 0, 1, 1],
            ['oxtrwsliderid', 'trwslider2object', 0, 1, 1],
        ],
    ];

    /**
     * Removes category from selected slider.
     *
     * @throws DatabaseConnectionException
     */
    public function removeCategoryFromSlider(): void
    {
        $oRequest = Registry::getConfig();

        $sViewName = 'trwslider2object';

        $aRemoveCategories = $this->_getActionIds($sViewName . '.oxid');
        if ($oRequest->getRequestParameter('all')) {
            $sQ = $this->_addFilter("delete {$sViewName}.* " . $this->_getQuery());
            ToolsDB::execute($sQ);
        } elseif ($aRemoveCategories && is_array($aRemoveCategories)) {
            $sQ = "delete from {$sViewName}
                where {$sViewName}.oxid in (" .
                implode(
                    ', ',
                    DatabaseProvider::getDb()->quoteArray($aRemoveCategories)
                )
                . ') ';
            ToolsDB::execute($sQ);
        }
    }

    /**
     * Adds Category to selected slider.
     *
     * @throws Exception
     */
    public function addCategoryToSlider(): void
    {
        $oRequest = Registry::getRequest();

        $sCategoryTable = 'oxcategories';

        $aAddCategories = $this->_getActionIds($sCategoryTable . '.oxid');

        $sSyncSliderOxId = $oRequest->getRequestParameter('synchoxid');
        if ($oRequest->getRequestParameter('all')) {
            $sCategoryTable = $this->_getViewName($sCategoryTable);
            $aAddCategories = $this->_getAll($this->_addFilter("select {$sCategoryTable}.oxid " . $this->_getQuery()));
        }

        if ($sSyncSliderOxId && $sSyncSliderOxId !== '-1' && is_array($aAddCategories)) {
            foreach ($aAddCategories as $sAddCategory) {
                $sOxId = ToolsDB::getAnyId(
                    'trwslider2object',
                    [
                        'oxtrwsliderid' => $sSyncSliderOxId,
                        'oxobjectid'    => $sAddCategory,
                        'oxclass'       => 'Category',
                    ]
                );

                // check if defaultpos is already selected
                $sPosDefault = ToolsDB::getAnyId(
                    'trwslider2object',
                    [
                        'oxtrwsliderid' => $sSyncSliderOxId,
                        'oxclass'       => 'Category',
                        'oxpos'         => 'mainheader',
                    ]
                );

                if (!$sOxId) {
                    $oTRWSlider2Object = oxNew(Slider2Object::class);
                    $aParams = [
                        'oxtrwsliderid' => $sSyncSliderOxId,
                        'oxobjectid'    => $sAddCategory,
                        'oxclass'       => 'Category',
                        'oxpos'         => ($sPosDefault ? '' : 'mainheader'),
                    ];
                    $aParams = ToolsDB::convertDB2OxParams($aParams, 'trwslider2object');
                    $oTRWSlider2Object->assign($aParams);
                    $oTRWSlider2Object->save();
                }
            }
        }
    }

    /**
     * Saves Slider value.
     *
     * @throws Exception
     */
    public function saveSliderValue(): void
    {
        $oRequest = Registry::getRequest();

        $this->resetContentCache();

        $sOxId = $oRequest->getRequestParameter('trwslider2object_oxid');
        $sSliderPos = $oRequest->getRequestParameter('slider_pos');

        $oSlider2Object = oxNew(Slider2Object::class);
        if ($sSliderPos && $oSlider2Object->load($sOxId)) {
            $oSlider2Object->trwslider2object__oxpos = new Field($sSliderPos);
            $oSlider2Object->save();
        } elseif (!$sSliderPos) {
            $oSlider2Object->delete($sOxId);
            $oSlider2Object->save();
        }
    }

    /**
     * Template getter get the possible Position of Sliders as Html Option for Selects.
     *
     * @throws DatabaseErrorException
     */
    public function getPosValuesAsHtmlOptionForSelect(): void
    {
        $oRequest = Registry::getRequest();

        $sResult = '';
        $aSliderPos = SliderHelper::getPosValues(
            $oRequest->getRequestParameter('oxtrwsliderid'),
            $oRequest->getRequestParameter('sclass'),
            $oRequest->getRequestParameter('oxobjectid')
        );

        foreach ($aSliderPos as $oSliderPos) {
            $sResult .= sprintf(
                '<option value="%s"%s>%s</option>',
                $oSliderPos->value,
                $oSliderPos->selected ? ' selected' : '',
                $oSliderPos->title
            );
        }
        echo $sResult;
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     *
     * @throws DatabaseConnectionException
     */
    protected function _getQuery()
    {
        $oRequest = Registry::getRequest();
        $oDb = DatabaseProvider::getDb();

        // looking for table/view
        $sCategoryTable = $this->_getViewName('oxcategories');
        $sViewName = $this->_getViewName('trwslider2object');

        $sSliderOxId = $oRequest->getRequestParameter('oxid');
        $sSyncSliderOxId = $oRequest->getRequestParameter('synchoxid');

        // Category selected or not?
        if (!$sSliderOxId) {
            $sQAdd = " from {$sCategoryTable} where 1 ";
        } else {
            $sQAdd = " from {$sCategoryTable}
                left join {$sViewName} on {$sViewName}.oxobjectid = {$sCategoryTable}.oxid
                where {$sViewName}.oxtrwsliderid = " . $oDb->quote($sSliderOxId) . "
                and {$sViewName}.oxclass = 'Category' ";
        }

        $sQAdd .= " and {$sCategoryTable}.oxactive = 1 ";

        if ($sSyncSliderOxId && $sSyncSliderOxId !== $sSliderOxId) {
            $sQAdd .= " and {$sCategoryTable}.`oxid` not in (
                    select {$sCategoryTable}.`oxid`
                    from {$sCategoryTable}
                    left join {$sViewName} on {$sViewName}.`oxobjectid` = {$sCategoryTable}.`oxid`
                    where {$sViewName}.`oxtrwsliderid` = " . $oDb->quote($sSyncSliderOxId) . "
                    and {$sViewName}.oxclass = 'Category'
                ) ";
        }

        return $sQAdd;
    }
}
