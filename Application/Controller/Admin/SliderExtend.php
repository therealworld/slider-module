<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SliderModule\Application\Controller\Admin;

use Exception;
use OxidEsales\Eshop\Application\Controller\Admin\AdminDetailsController;
use OxidEsales\Eshop\Core\Registry;
use stdClass;
use TheRealWorld\SliderModule\Application\Model\Slider;
use TheRealWorld\SliderModule\Application\Model\SliderElement;
use TheRealWorld\SliderModule\Application\Model\SliderElementList;
use TheRealWorld\ToolsPlugin\Core\ToolsDB;

/**
 * provides a Slider-Configuration
 * Admin Menu: Customer Information -> Slider Settings.
 */
class SliderExtend extends AdminDetailsController
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_sThisTemplate = 'sliderextend.tpl';

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function render()
    {
        parent::render();

        $sOxId = $this->_aViewData['oxid'] = $this->getEditObjectId();
        $oRequest = Registry::getRequest();
        $oConfig = Registry::getConfig();
        $iAoc = $oRequest->getRequestParameter('aoc');

        if ($sOxId !== '-1') {
            // load object
            $oTRWSlider = oxNew(Slider::class);
            $oTRWSlider->load($sOxId);

            // Disable editing for derived items
            if ($oTRWSlider->isDerived()) {
                $this->_aViewData['readonly'] = true;
            }
            if (!$iAoc) {
                // load SliderElements
                $oTRWSliderElementList = oxNew(SliderElementList::class);
                $oTRWSliderElementList->loadElementsForSliderAdmin($sOxId);

                foreach ($oTRWSliderElementList as $oTRWSliderElement) {
                    $sElOxId = $oTRWSliderElement->getId();
                    $oTRWSliderElement->setLanguage($this->_iEditLang);
                    $oTRWSliderElement->loadInLang($this->_iEditLang, $sElOxId);
                    $this->_aViewData['aElements'][$sElOxId] = clone $oTRWSliderElement;

                    if ($oConfig->getConfigParam('bTRWSliderUseWYSIWYG')) {
                        $sEditor = $this->generateTextEditor(
                            '100%',
                            300,
                            $oTRWSliderElement,
                            'trwsliderelements__oxdesc',
                            'details.tpl.css'
                        );
                        $this->_aViewData['aEditors'][$sElOxId] = str_replace(
                            'editor_trwsliderelements__oxdesc',
                            'editor_trwsliderelements__oxdesc_' . $sElOxId,
                            $sEditor
                        );
                    }
                }

                $aLanguages = Registry::getLang()->getLanguageNames();
                foreach ($aLanguages as $id => $sLanguage) {
                    $oOtherLang = new stdClass();
                    $oOtherLang->sLangDesc = $sLanguage;
                    $oOtherLang->selected = ($id === $this->_iEditLang);
                    $this->_aViewData['otherlang'][$id] = clone $oOtherLang;
                }
                $this->_aViewData['sTRWSliderPicSize'] = $oConfig->getConfigParam('sTRWSliderPicSize');
                $this->_aViewData['sTRWSliderPicAlternativeSize'] = $oConfig->getConfigParam('sTRWSliderPicAlternativeSize');
            } else {
                switch ($iAoc) {
                    // Article
                    case '1':
                        $oSliderAjax = oxNew(SliderelementArticleAjax::class);
                        $this->_aViewData['oxajax'] = $oSliderAjax->getColumns();

                        return 'popupsliderelementarticle.tpl';

                        // Categories
                    case '2':
                        $oSliderAjax = oxNew(SliderelementCategoryAjax::class);
                        $this->_aViewData['oxajax'] = $oSliderAjax->getColumns();

                        return 'popupsliderelementcategory.tpl';
                }
            }
        }

        return $this->_sThisTemplate;
    }

    /** add New Element to the Slider.
     * @throws Exception
     */
    public function addNewElement(): void
    {
        $sOxId = $this->getEditObjectId();
        $oTRWSliderElement = oxNew(SliderElement::class);
        $aParams = [
            'oxactive'      => 1,
            'oxtrwsliderid' => $sOxId,
        ];
        $aParams = ToolsDB::convertDB2OxParams($aParams, 'trwsliderelements');
        $oTRWSliderElement->assign($aParams);
        $oTRWSliderElement->save();
    }

    /** delete a Element from the Slider */
    public function deleteElement(): void
    {
        if ($sDeleteElement = Registry::getRequest()->getRequestParameter('sDeleteElement')) {
            $oTRWSliderElement = oxNew(SliderElement::class);
            $oTRWSliderElement->load($sDeleteElement);
            $oTRWSliderElement->delete();
        }
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function save()
    {
        parent::save();

        $oRequest = Registry::getRequest();

        $aParamsVal = $oRequest->getRequestParameter('editval');
        $aParamsArr = $oRequest->getRequestParameter('editvalarr');
        $aSort = array_flip($oRequest->getRequestParameter('sort'));

        foreach ($aParamsArr as $sOxId => $aParams) {
            $aParams['trwsliderelements__oxsort'] = (int) $aSort[$sOxId] + 1;
            $oTRWSliderElement = oxNew(SliderElement::class);
            $oTRWSliderElement->setLanguage($this->_iEditLang);
            $oTRWSliderElement->loadInLang($this->_iEditLang, $sOxId);

            // longdesc
            $sKey = 'trwsliderelements__oxdesc_' . $sOxId;
            if (is_array($aParamsVal) && array_key_exists($sKey, $aParamsVal)) {
                $aParams['trwsliderelements__oxdesc'] = $aParamsVal[$sKey];
            }

            // maybe picture delete?
            if (
                empty($aParams['trwsliderelements__oxpic'])
                && $oTRWSliderElement->getTRWStringData('oxpic')
            ) {
                $oTRWSliderElement->deletePic();
            }
            if (
                empty($aParams['trwsliderelements__oxpicalternative'])
                && $oTRWSliderElement->getTRWStringData('oxpicalternative')
            ) {
                $oTRWSliderElement->deletePicAlternative();
            }

            $oTRWSliderElement->assign($aParams);
            $aFiles = [
                'myfile' => $_FILES['myfile_' . $sOxId],
            ];
            $oTRWSliderElement = Registry::getUtilsFile()->processFiles(
                $oTRWSliderElement,
                $aFiles
            );

            $oTRWSliderElement->save();
        }
    }
}
