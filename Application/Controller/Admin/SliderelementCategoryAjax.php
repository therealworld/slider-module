<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SliderModule\Application\Controller\Admin;

use Exception;
use OxidEsales\Eshop\Application\Controller\Admin\ListComponentAjax;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
use OxidEsales\Eshop\Core\Registry;
use TheRealWorld\SliderModule\Application\Model\SliderElement2Object;
use TheRealWorld\ToolsPlugin\Core\ToolsDB;

/**
 * Class manages sliderelement assignment to categories.
 */
class SliderelementCategoryAjax extends ListComponentAjax
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_aColumns = [
        'container1' => [ // field , table,  visible, multilanguage, ident
            ['oxtitle', 'oxcategories', 1, 1, 0],
            ['oxid', 'oxcategories', 0, 0, 1],
        ],
        'container2' => [
            ['oxtitle', 'oxcategories', 1, 1, 0],
            ['oxid', 'oxcategories', 0, 0, 1],
            ['oxid', 'trwsliderelements2object', 0, 0, 1],
        ],
    ];

    /** Removes category from selected sliderelement.
     * @throws DatabaseConnectionException
     */
    public function removeCategoryFromSliderElement(): void
    {
        $oRequest = Registry::getRequest();

        $sViewName = 'trwsliderelements2object';

        $aRemoveCategories = $this->_getActionIds($sViewName . '.oxid');
        if ($oRequest->getRequestParameter('all')) {
            $sQ = $this->_addFilter("delete {$sViewName}.* " . $this->_getQuery());
            ToolsDB::execute($sQ);
        } elseif ($aRemoveCategories && is_array($aRemoveCategories)) {
            $sQ = "delete from {$sViewName}
                where {$sViewName}.oxid in (" .
                implode(
                    ', ',
                    DatabaseProvider::getDb()->quoteArray($aRemoveCategories)
                )
                . ') ';
            ToolsDB::execute($sQ);
        }
    }

    /** Adds Category to selected sliderelement.
     * @throws Exception
     */
    public function addCategoryToSliderElement(): void
    {
        $oRequest = Registry::getRequest();

        $sCategoryTable = 'oxcategories';

        $aAddCategories = $this->_getActionIds($sCategoryTable . '.oxid');

        $sSyncSliderElementOxId = $oRequest->getRequestParameter('synchoxid');
        if ($oRequest->getRequestParameter('all')) {
            $sCategoryTable = $this->_getViewName($sCategoryTable);
            $aAddCategories = $this->_getAll($this->_addFilter("select {$sCategoryTable}.oxid " . $this->_getQuery()));
        }

        if ($sSyncSliderElementOxId && $sSyncSliderElementOxId !== '-1' && is_array($aAddCategories)) {
            $bFirst = true;
            foreach ($aAddCategories as $sAddCategory) {
                // thereby we allow only one mapping
                if ($bFirst) {
                    $bFirst = false;
                    $oTRWSliderElement2Object = oxNew(SliderElement2Object::class);
                    $oTRWSliderElement2Object->cleanElement2Object($sSyncSliderElementOxId, $sAddCategory);

                    $sOxId = ToolsDB::getAnyId(
                        'trwsliderelements2object',
                        [
                            'oxtrwsliderelementid' => $sSyncSliderElementOxId,
                            'oxobjectid'           => $sAddCategory,
                            'oxclass'              => 'Category',
                        ]
                    );
                    if (!$sOxId) {
                        $oTRWSlider2Object = oxNew(SliderElement2Object::class);
                        $aParams = [
                            'oxtrwsliderelementid' => $sSyncSliderElementOxId,
                            'oxobjectid'           => $sAddCategory,
                            'oxclass'              => 'Category',
                        ];
                        $aParams = ToolsDB::convertDB2OxParams($aParams, 'trwsliderelements2object');
                        $oTRWSlider2Object->assign($aParams);
                        $oTRWSlider2Object->save();
                    }
                }
            }
        }
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     *
     * @throws DatabaseConnectionException
     */
    protected function _getQuery()
    {
        $oDb = DatabaseProvider::getDb();
        $oRequest = Registry::getRequest();

        // looking for table/view
        $sCategoryTable = $this->_getViewName('oxcategories');
        $sViewName = $this->_getViewName('trwsliderelements2object');

        $sSliderElementOxId = $oRequest->getRequestParameter('oxid');
        $sSyncSliderElementOxId = $oRequest->getRequestParameter('synchoxid');

        // Category selected or not?
        if (!$sSliderElementOxId) {
            $sQAdd = " from {$sCategoryTable} where 1 ";
        } else {
            $sQAdd = " from {$sCategoryTable}
                left join {$sViewName} on {$sViewName}.oxobjectid = {$sCategoryTable}.oxid
                where {$sViewName}.oxtrwsliderelementid = " . $oDb->quote($sSliderElementOxId) . "
                and {$sViewName}.oxclass = 'Category' ";
        }

        $sQAdd .= " and {$sCategoryTable}.oxactive = 1 ";

        if ($sSyncSliderElementOxId && $sSyncSliderElementOxId !== $sSliderElementOxId) {
            $sQAdd .= " and {$sCategoryTable}.oxid not in (
                    select {$sCategoryTable}.oxid
                    from {$sCategoryTable}
                    left join {$sViewName} on {$sViewName}.oxobjectid = {$sCategoryTable}.oxid
                    where {$sViewName}.oxtrwsliderelementid = " . $oDb->quote($sSyncSliderElementOxId) . "
                    and {$sViewName}.oxclass = 'Category'
                ) ";
        }

        return $sQAdd;
    }
}
