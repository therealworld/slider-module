<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SliderModule\Application\Controller;

use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
use TheRealWorld\SliderModule\Application\Model\Slider;

/**
 * article details class.
 *
 * @mixin \OxidEsales\Eshop\Application\Controller\ArticleDetailsController
 */
class ArticleDetailsController extends ArticleDetailsController_parent
{
    /** Array with Slider Objects */
    protected array $_aSlider = [
        'pageheader' => null,
        'mainheader' => null,
        'mainfooter' => null,
    ];

    /** Template variable getter. Returns PageHeader slider */
    public function getPageHeaderSlider(): ?Slider
    {
        return $this->_getSlider('pageheader');
    }

    /** Template variable getter. Returns PageHeader Alternative slider */
    public function getPageHeaderAlternativeSlider(): ?Slider
    {
        return $this->_getSlider('pageheaderalternative');
    }

    /** Template variable getter. Returns MainHeader slider */
    public function getMainHeaderSlider(): ?Slider
    {
        return $this->_getSlider('mainheader');
    }

    /** Template variable getter. Returns MainFooter slider */
    public function getMainFooterSlider(): ?Slider
    {
        return $this->_getSlider('mainfooter');
    }

    /** get the slider by Position.
     *
     * @param string $sPos - Position of Slider
     *
     * @throws DatabaseConnectionException
     */
    protected function _getSlider(string $sPos = ''): ?Slider
    {
        if (is_null($this->_aSlider[$sPos])) {
            $oProduct = $this->getProduct();
            $oTRWSlider = oxNew(Slider::class);
            if ($oTRWSlider->loadSlider('Article', $oProduct->getId(), $sPos)) {
                $this->_aSlider[$sPos] = $oTRWSlider;
            }
        }

        return $this->_aSlider[$sPos];
    }
}
