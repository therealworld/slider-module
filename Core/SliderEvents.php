<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SliderModule\Core;

use OxidEsales\Eshop\Core\DbMetaDataHandler;
use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
use OxidEsales\Eshop\Core\Exception\DatabaseErrorException;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use TheRealWorld\ToolsPlugin\Core\ToolsConfig;
use TheRealWorld\ToolsPlugin\Core\ToolsDB;

class SliderEvents
{
    /**
     * OXID-Core.
     *
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws DatabaseConnectionException
     * @throws DatabaseErrorException
     */
    public static function onActivate()
    {
        $sCreateSql = "create table if not exists `trwslider` (
            `OXID` char(32) character set latin1 collate latin1_general_ci not null,
            `OXSHOPID` int(11) NOT NULL DEFAULT '1' COMMENT 'Shop id (oxshops)',
            `OXACTIVE` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Active',
            `OXTITLE` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Slider Title',
            `OXDATEFROM` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Slider Date from',
            `OXDATETO` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Slider Date to',
            `OXTIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Timestamp',
            primary key (`OXID`)
            ) engine=InnoDB default CHARSET=utf8 comment 'the-real-world: Sliders'";
        ToolsDB::execute($sCreateSql);

        // additional Module-Update v1.7
        if (!ToolsDB::tableColumnExists('trwslider', 'OXSHOPID')) {
            $sAddSql = "ALTER TABLE `trwslider` ADD `OXSHOPID` int(11) NOT NULL DEFAULT '1' COMMENT 'Shop id (oxshops)'";
            ToolsDB::execute($sAddSql);
        }

        $sCreateSql = "create table if not exists `trwslider2object` (
            `OXID` char(32) COLLATE latin1_general_ci NOT NULL COMMENT 'Record id',
            `OXTRWSLIDERID` char(32) COLLATE latin1_general_ci NOT NULL DEFAULT '' COMMENT 'Slider id (trwslider)',
            `OXOBJECTID` char(32) COLLATE latin1_general_ci NOT NULL DEFAULT '' COMMENT 'Object id (table set by oxclass)',
            `OXCLASS` char(32) COLLATE latin1_general_ci NOT NULL DEFAULT '' COMMENT 'Object table name',
            `OXPOS` enum('pageheader','pageheaderalternative','mainheader','mainfooter') collate utf8_general_ci NOT NULL DEFAULT 'mainheader' COMMENT 'Position of slider',
            `OXTIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Timestamp',
            PRIMARY KEY (`OXID`),
            KEY `OXTRWSLIDERID` (`OXTRWSLIDERID`),
            KEY `OXOBJECTID` (`OXOBJECTID`,`OXCLASS`)
            ) ENGINE=InnoDB default CHARSET=utf8 comment='the-real-world: Shows many-to-many relationship between sliders (trwslider) and objects (table set by oxclass)';";
        ToolsDB::execute($sCreateSql);

        $sCreateSql = "create table if not exists `trwsliderelements` (
            `OXID` char(32) COLLATE latin1_general_ci NOT NULL COMMENT 'Record id',
            `OXTRWSLIDERID` char(32) COLLATE latin1_general_ci NOT NULL DEFAULT '' COMMENT 'Slider id (trwslider)',
            `OXACTIVE` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Active',
            `OXDATEFROM` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'SliderElement Date from',
            `OXDATETO` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'SliderElement Date to',
            `OXTITLE` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT 'SliderElement Title (multilanguage)',
            `OXTITLE_1` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT '',
            `OXTITLE_2` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT '',
            `OXTITLE_3` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT '',
            `OXDESC` text collate utf8_general_ci NOT NULL DEFAULT '' COMMENT 'SliderElement Description (multilanguage)',
            `OXDESC_1` text collate utf8_general_ci NOT NULL DEFAULT '' COMMENT '',
            `OXDESC_2` text collate utf8_general_ci NOT NULL DEFAULT '' COMMENT '',
            `OXDESC_3` text collate utf8_general_ci NOT NULL DEFAULT '' COMMENT '',
            `OXPIC` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT 'SliderElement Picture',
            `OXPICALTERNATIVE` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT 'SliderElement Picture Alternative',
            `OXPICTITLE` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT 'SliderElement Picture Title (multilanguage)',
            `OXPICTITLE_1` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT '',
            `OXPICTITLE_2` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT '',
            `OXPICTITLE_3` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT '',
            `OXOBJECTID` char(32) COLLATE latin1_general_ci NOT NULL DEFAULT '' COMMENT 'SliderElement Link to Object id (table set by oxclass)',
            `OXCLASS` char(32) COLLATE latin1_general_ci NOT NULL DEFAULT '' COMMENT 'SliderElement Link to Object table name',
            `OXLINK` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT 'SliderElement Link',
            `OXLINKTARGET` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT 'SliderElement LinkTarget',
            `OXBGCOLOR` char(6) NOT NULL DEFAULT '' COMMENT 'SliderElement BGColor-Code',
            `OXBTNCOLOR` char(6) NOT NULL DEFAULT '' COMMENT 'SliderElement Button-Color-Code',
            `OXBTNBGCOLOR` char(6) NOT NULL DEFAULT '' COMMENT 'SliderElement Button-BGColor-Code',
            `OXFULLWIDTH` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Sliderelement with Fullwidth',
            `OXSORT` int(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Sortposition',
            `OXTIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Timestamp',
            PRIMARY KEY (`OXID`),
            KEY `OXTRWSLIDERID` (`OXTRWSLIDERID`),
            KEY `OXOBJECTID` (`OXOBJECTID`,`OXCLASS`)
            ) ENGINE=InnoDB default CHARSET=utf8 comment='the-real-world: Shows many-to-many relationship between sliders (trwslider) and elements';";
        ToolsDB::execute($sCreateSql);

        // additional Module-Update v1.9
        if (!ToolsDB::tableColumnExists('trwsliderelements', 'OXPICALTERNATIVE')) {
            $sAddSql = "ALTER TABLE `trwsliderelements` ADD `OXPICALTERNATIVE` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT 'SliderElement Picture Alternative'";
            ToolsDB::execute($sAddSql);
        }
        if (!ToolsDB::tableColumnExists('trwsliderelements', 'OXLINKTARGET')) {
            $sAddSql = "ALTER TABLE `trwsliderelements` ADD `OXLINKTARGET` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT 'SliderElement LinkTarget'";
            ToolsDB::execute($sAddSql);
        }

        $sCreateSql = "create table if not exists `trwsliderelements2object` (
            `OXID` char(32) COLLATE latin1_general_ci NOT NULL COMMENT 'Record id',
            `OXTRWSLIDERELEMENTID` char(32) COLLATE latin1_general_ci NOT NULL DEFAULT '' COMMENT 'Sliderelement id (trwsliderelements)',
            `OXOBJECTID` char(32) COLLATE latin1_general_ci NOT NULL DEFAULT '' COMMENT 'Object id (table set by oxclass)',
            `OXCLASS` char(32) COLLATE latin1_general_ci NOT NULL DEFAULT '' COMMENT 'Object table name',
            `OXTIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Timestamp',
            PRIMARY KEY (`OXID`),
            KEY `OXTRWSLIDERELEMENTID` (`OXTRWSLIDERELEMENTID`),
            KEY `OXOBJECTID` (`OXOBJECTID`,`OXCLASS`)
            ) ENGINE=InnoDB default CHARSET=utf8 comment='the-real-world: Shows many-to-many relationship between sliderelements (trwsliderelements) and objects (table set by oxclass)';";
        ToolsDB::execute($sCreateSql);

        if (ToolsConfig::isMultiShop()) {
            $sCreateSql = "create table if not exists `trwslider2shop` (
                  `OXSHOPID` int(11) NOT NULL COMMENT 'Mapped shop id',
                  `OXMAPOBJECTID` bigint(20) NOT NULL COMMENT 'Mapped object id',
                  `OXTIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Timestamp',
                  UNIQUE KEY `OXMAPIDX` (`OXSHOPID`,`OXMAPOBJECTID`),
                  KEY `OXMAPOBJECTID` (`OXMAPOBJECTID`),
                  KEY `OXSHOPID` (`OXSHOPID`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Mapping table for element subshop assignments';
                ";
            ToolsDB::execute($sCreateSql);

            if (!ToolsDB::tableColumnExists('trwslider', 'OXMAPID')) {
                $sAddSql = "ALTER TABLE `trwslider` ADD `OXMAPID` bigint(20) NOT NULL COMMENT 'Integer mapping identifier'";
                ToolsDB::execute($sAddSql);
            }

            if (!ToolsDB::tableIndexExists('trwslider', 'OXMAPID')) {
                $sAddSql = "ALTER TABLE `trwslider` ADD INDEX `OXMAPID` (`OXMAPID`),
                    CHANGE `OXMAPID` `OXMAPID` bigint(20) NOT NULL COMMENT 'Integer mapping identifier' AUTO_INCREMENT";
                ToolsDB::execute($sAddSql);
            }
        }

        ToolsDB::setTableMultiShop('trwslider');
        ToolsDB::setTableMultiLang('trwsliderelements');

        $oMetaData = oxNew(DbMetaDataHandler::class);
        $oMetaData->updateViews();

        // add the Dynamic Image Configurations Options to the config
        ToolsConfig::addOptionToShopConfigArray(
            'aTRWToolsDynImgConfParamToPath',
            'trwtools',
            [
                'sTRWSliderPicSize'            => '/.*\/generated\/slider\/\d+\_\d+\_\d+$/',
                'sTRWSliderPicAlternativeSize' => '/.*\/generated\/slider_alternative\/\d+\_\d+\_\d+$/',
            ],
            'aarr'
        );

        ToolsConfig::addOptionToShopConfigArray(
            'aTRWToolsDynImgDetectWithTwoOptions',
            'trwtools',
            [
                '/slider/',
                '/slider_alternative/',
            ]
        );

        return true;
    }

    /**
     * OXID-Core.
     *
     * @throws ContainerExceptionInterface
     * @throws DatabaseConnectionException
     * @throws DatabaseErrorException
     * @throws NotFoundExceptionInterface
     */
    public static function onDeactivate()
    {
        // for security set return true
        return true;
        if ($aViews = ToolsDB::getTableWithPrefix('oxv_trwslider')) {
            foreach ($aViews as $sView) {
                $sDeleteSql = "drop view `{$sView}`";
                ToolsDB::execute($sDeleteSql);
            }
        }

        ToolsDB::deleteTableMultiShop('trwslider');
        ToolsDB::deleteTableMultiLang('trwsliderelements');

        $sDeleteSql = 'drop table if exists `trwslider`';
        ToolsDB::execute($sDeleteSql);

        $sDeleteSql = 'drop table if exists `trwslider2shop`';
        ToolsDB::execute($sDeleteSql);

        $sDeleteSql = 'drop table if exists `trwslider2object`';
        ToolsDB::execute($sDeleteSql);

        $sDeleteSql = 'drop table if exists `trwsliderelements`';
        ToolsDB::execute($sDeleteSql);

        $sDeleteSql = 'drop table if exists `trwsliderelements2object`';
        ToolsDB::execute($sDeleteSql);

        return true;
    }
}
