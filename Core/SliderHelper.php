<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SliderModule\Core;

use OxidEsales\Eshop\Core\Exception\DatabaseErrorException;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Str;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use stdClass;
use TheRealWorld\ToolsPlugin\Core\ToolsConfig;
use TheRealWorld\ToolsPlugin\Core\ToolsDB;

class SliderHelper
{
    /** default Position of Sliders for a Controller (important, if we have a missconfiguration) */
    protected static array $_aDefaultSliderPos = [
        'Start'    => 'pageheader',
        'Article'  => 'mainheader',
        'Category' => 'mainheader',
        'Content'  => 'mainheader',
    ];

    /** possible Slider Positions */
    protected static ?array $_aSliderPos = null;

    /**
     * Get the possible Positions of Sliders for a Controller.
     *
     * @param string $sOxId  - OxId of SliderElement
     * @param string $sClass - possible Controllers for Slider
     *
     * @throws DatabaseErrorException
     */
    public static function getPosValues(string $sOxId, string $sClass, string $sObjectId = ''): array
    {
        if (is_null(self::$_aSliderPos)) {
            $oLang = Registry::getLang();
            self::$_aSliderPos = [];

            if (array_key_exists($sClass, self::getListOfDefaultPos())) {
                // first dummy line
                $oItem = new stdClass();
                $oItem->title = $oLang->translateString('ARTICLE_MAIN_NONE');
                $oItem->value = '';
                $oItem->selected = false;
                self::$_aSliderPos[] = clone $oItem;

                // get selected Pos
                $sSelectedPos = ToolsDB::getAnyId(
                    'trwslider2object',
                    [
                        'oxclass'       => $sClass,
                        'oxtrwsliderid' => $sOxId,
                        'oxobjectid'    => $sObjectId,
                    ],
                    'oxpos'
                );

                $aPos = ToolsDB::showEnumValues('trwslider2object', 'oxpos');
                foreach ($aPos as $sPos) {
                    $bShow = Registry::getConfig()->getConfigParam('bTRWSliderPos' . $sPos);

                    $sExistingOxId = ToolsDB::getAnyId(
                        'trwslider2object',
                        [
                            'oxclass'    => $sClass,
                            'oxpos'      => $sPos,
                            'oxobjectid' => $sObjectId,
                        ],
                        'oxtrwsliderid'
                    );

                    if ($sExistingOxId && ($sExistingOxId !== $sOxId)) {
                        $bShow = false;
                    }

                    if ($bShow) {
                        $oItem->title = $oLang->translateString('TRWSLIDER_OXPOS_' . Str::getStr()->strtoupper($sPos));
                        $oItem->value = $sPos;
                        $oItem->selected = $sSelectedPos === $sPos;
                        self::$_aSliderPos[$sPos] = clone $oItem;
                    }
                }
            }
        }

        return self::$_aSliderPos;
    }

    /**
     * Get the default Position of Sliders for a Controller.
     *
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function getDefaultPos(string $sClass): string
    {
        $oConfig = Registry::getConfig();

        $sPos = $oConfig->getConfigParam('sTRWSliderDefault' . $sClass);
        if (!$oConfig->getConfigParam('bTRWSliderPos' . $sPos)) {
            $sPos = self::$_aDefaultSliderPos[$sClass];
            ToolsConfig::saveConfigParam('sTRWSliderDefault' . $sClass, $sPos, 'str', 'trwslider');
        }

        return $sPos;
    }

    /** Get list of default Position of Sliders for a Controller */
    public static function getListOfDefaultPos(): array
    {
        return self::$_aDefaultSliderPos;
    }
}
