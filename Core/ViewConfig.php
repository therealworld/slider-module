<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SliderModule\Core;

use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
use TheRealWorld\SliderModule\Application\Model\Slider;

/**
 * View config data access class. Keeps most
 * of getters needed for formatting various urls,
 * config parameters, session information etc.
 */
class ViewConfig extends ViewConfig_parent
{
    /** Slider Object */
    protected ?Slider $_oFooterSlider = null;

    /** get the slider for Footer.
     * @throws DatabaseConnectionException
     */
    public function getFooterSlider(): ?Slider
    {
        if (is_null($this->_oFooterSlider)) {
            $oTRWSlider = oxNew(Slider::class);
            if ($oTRWSlider->loadSlider('Footer', '')) {
                $this->_oFooterSlider = $oTRWSlider;
            }
        }

        return $this->_oFooterSlider;
    }
}
