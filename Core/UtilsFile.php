<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\SliderModule\Core;

/**
 * File manipulation utility class.
 */
class UtilsFile extends UtilsFile_parent
{
    /** Slider images upload dir name */
    protected const TRWSLIDER_PICTURE_DIR = 'master/slider';

    /** Slider images upload dir name */
    protected const TRWSLIDER_PICTURE_ALTERNATIVE_DIR = 'master/slider_alternative';

    /** Image type and its folder information array */
    protected array $_aAddSliderTypeToPath = [
        'TRWSLIDER'            => self::TRWSLIDER_PICTURE_DIR,
        'TRWSLIDERALTERNATIVE' => self::TRWSLIDER_PICTURE_ALTERNATIVE_DIR,
    ];

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function __construct()
    {
        parent::__construct();
        $this->_aTypeToPath = array_merge(
            $this->_aTypeToPath,
            $this->_aAddSliderTypeToPath
        );
    }

    /** return the normalized Slider Picture Dir */
    public function normalizeSliderPictureDir(): string
    {
        return $this->normalizeDir(self::TRWSLIDER_PICTURE_DIR);
    }

    /** return the normalized Slider Picture Alternative Dir */
    public function normalizeSliderPictureAlternativeDir(): string
    {
        return $this->normalizeDir(self::TRWSLIDER_PICTURE_ALTERNATIVE_DIR);
    }
}
