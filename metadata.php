<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// Metadata version.

use OxidEsales\Eshop\Application\Controller\ArticleDetailsController as OxArticleDetailsController;
use OxidEsales\Eshop\Application\Controller\ArticleListController as OxArticleListController;
use OxidEsales\Eshop\Application\Controller\StartController as OxStartController;
use OxidEsales\Eshop\Application\Model\Article as OxArticle;
use OxidEsales\Eshop\Application\Model\Category as OxCategory;
use OxidEsales\Eshop\Application\Model\Content as OxContent;
use OxidEsales\Eshop\Core\Language as OxLanguage;
use OxidEsales\Eshop\Core\UtilsFile as OxUtilsFile;
use OxidEsales\Eshop\Core\ViewConfig as OxViewConfig;
use TheRealWorld\SliderModule\Application\Controller\Admin\SliderArticleAjax;
use TheRealWorld\SliderModule\Application\Controller\Admin\SliderCategoryAjax;
use TheRealWorld\SliderModule\Application\Controller\Admin\SliderContentAjax;
use TheRealWorld\SliderModule\Application\Controller\Admin\SliderController;
use TheRealWorld\SliderModule\Application\Controller\Admin\SliderelementArticleAjax;
use TheRealWorld\SliderModule\Application\Controller\Admin\SliderelementCategoryAjax;
use TheRealWorld\SliderModule\Application\Controller\Admin\SliderExtend;
use TheRealWorld\SliderModule\Application\Controller\Admin\SliderList;
use TheRealWorld\SliderModule\Application\Controller\Admin\SliderMain;
use TheRealWorld\SliderModule\Application\Controller\ArticleDetailsController;
use TheRealWorld\SliderModule\Application\Controller\ArticleListController;
use TheRealWorld\SliderModule\Application\Controller\StartController;
use TheRealWorld\SliderModule\Application\Model\Article;
use TheRealWorld\SliderModule\Application\Model\Category;
use TheRealWorld\SliderModule\Application\Model\Content;
use TheRealWorld\SliderModule\Core\Language;
use TheRealWorld\SliderModule\Core\UtilsFile;
use TheRealWorld\SliderModule\Core\ViewConfig;
use TheRealWorld\ToolsPlugin\Core\ToolsModuleVersion;

$sMetadataVersion = '2.1';

/**
 * Module information.
 */
$aModule = [
    'id'    => 'trwslider',
    'title' => [
        'de' => 'the-real-world - Slidermodul',
        'en' => 'the-real-world - Slidermodule',
    ],
    'description' => [
        'de' => 'Slider für Artikel, Kategorien und Content-Seiten.',
        'en' => 'Slider for article, categories and content pages.',
    ],
    'thumbnail' => 'picture.png',
    'version'   => ToolsModuleVersion::getModuleVersion('trwslider'),
    'author'    => 'Mario Lorenz',
    'url'       => 'https://www.the-real-world.de',
    'email'     => 'mario_lorenz@the-real-world.de',
    'events'    => [
        'onActivate'   => '\TheRealWorld\SliderModule\Core\SliderEvents::onActivate',
        'onDeactivate' => '\TheRealWorld\SliderModule\Core\SliderEvents::onDeactivate',
    ],
    'extend' => [
        // Core
        OxLanguage::class   => Language::class,
        OxUtilsFile::class  => UtilsFile::class,
        OxViewConfig::class => ViewConfig::class,
        // Controller
        OxArticleDetailsController::class => ArticleDetailsController::class,
        OxArticleListController::class    => ArticleListController::class,
        OxStartController::class          => StartController::class,
        OxArticle::class                  => Article::class,
        OxCategory::class                 => Category::class,
        OxContent::class                  => Content::class,
    ],
    'controllers' => [
        'SliderController'           => SliderController::class,
        'SliderExtend'               => SliderExtend::class,
        'SliderMain'                 => SliderMain::class,
        'SliderList'                 => SliderList::class,
        'slidercategory_ajax'        => SliderCategoryAjax::class,
        'slidercontent_ajax'         => SliderContentAjax::class,
        'sliderarticle_ajax'         => SliderArticleAjax::class,
        'sliderelementcategory_ajax' => SliderelementCategoryAjax::class,
        'sliderelementarticle_ajax'  => SliderelementArticleAjax::class,
    ],
    'blocks' => [
        [
            'template' => 'bottomnaviitem.tpl',
            'block'    => 'admin_bottomnavicustom',
            'file'     => 'Application/views/blocks/admin_bottomnavicustom.tpl',
        ],
        [
            'template' => 'actions_main.tpl',
            'block'    => 'admin_actions_main_form',
            'file'     => 'Application/views/blocks/admin_actions_main_form.tpl',
            'position' => 0,
        ],
        [
            'template' => 'headitem.tpl',
            'block'    => 'admin_headitem_inccss',
            'file'     => 'Application/views/blocks/admin_headitem_inccss.tpl',
        ],
        [
            'template' => 'headitem.tpl',
            'block'    => 'admin_headitem_incjs',
            'file'     => 'Application/views/blocks/admin_headitem_incjs.tpl',
        ],
        [
            'template' => 'shop_mall.tpl',
            'block'    => 'admin_shop_mall_inheritance',
            'file'     => 'Application/views/blocks/admin_shop_mall_inheritance.tpl',
        ],
    ],
    'templates' => [
        'slider.tpl'                     => 'trw/trwslider/Application/views/admin/tpl/slider.tpl',
        'sliderextend.tpl'               => 'trw/trwslider/Application/views/admin/tpl/sliderextend.tpl',
        'slidermain.tpl'                 => 'trw/trwslider/Application/views/admin/tpl/slidermain.tpl',
        'sliderlist.tpl'                 => 'trw/trwslider/Application/views/admin/tpl/sliderlist.tpl',
        'popupsliderarticle.tpl'         => 'trw/trwslider/Application/views/admin/tpl/popups/popupsliderarticle.tpl',
        'popupslidercategory.tpl'        => 'trw/trwslider/Application/views/admin/tpl/popups/popupslidercategory.tpl',
        'popupslidercontent.tpl'         => 'trw/trwslider/Application/views/admin/tpl/popups/popupslidercontent.tpl',
        'popupsliderelementarticle.tpl'  => 'trw/trwslider/Application/views/admin/tpl/popups/popupsliderelementarticle.tpl',
        'popupsliderelementcategory.tpl' => 'trw/trwslider/Application/views/admin/tpl/popups/popupsliderelementcategory.tpl',
    ],
    'settings' => [
        [
            'group' => 'trwslidercontroller',
            'name'  => 'bTRWSliderShowCtrlStart',
            'type'  => 'bool',
            'value' => true,
        ],
        [
            'group' => 'trwslidercontroller',
            'name'  => 'bTRWSliderShowCtrlArticle',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group' => 'trwslidercontroller',
            'name'  => 'bTRWSliderShowCtrlCategory',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group' => 'trwslidercontroller',
            'name'  => 'bTRWSliderShowCtrlContent',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group' => 'trwslidercontroller',
            'name'  => 'bTRWSliderShowCtrlFooter',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group' => 'trwsliderposition',
            'name'  => 'bTRWSliderPosmainheader',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group' => 'trwsliderposition',
            'name'  => 'bTRWSliderPospageheader',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group' => 'trwsliderposition',
            'name'  => 'bTRWSliderPospageheaderalternative',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group' => 'trwsliderposition',
            'name'  => 'bTRWSliderPosmainfooter',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group'       => 'trwsliderdefault',
            'name'        => 'sTRWSliderDefaultStart',
            'type'        => 'select',
            'value'       => 'pageheader',
            'constraints' => 'pageheader|pageheaderalternative|mainheader|mainfooter',
        ],
        [
            'group'       => 'trwsliderdefault',
            'name'        => 'sTRWSliderDefaultArticle',
            'type'        => 'select',
            'value'       => 'mainheader',
            'constraints' => 'pageheader|pageheaderalternative|mainheader|mainfooter',
        ],
        [
            'group'       => 'trwsliderdefault',
            'name'        => 'sTRWSliderDefaultCategory',
            'type'        => 'select',
            'value'       => 'mainheader',
            'constraints' => 'pageheader|pageheaderalternative|mainheader|mainfooter',
        ],
        [
            'group'       => 'trwsliderdefault',
            'name'        => 'sTRWSliderDefaultContent',
            'type'        => 'select',
            'value'       => 'mainheader',
            'constraints' => 'pageheader|pageheaderalternative|mainheader|mainfooter',
        ],
        [
            'group' => 'trwslideroptions',
            'name'  => 'bTRWSliderUseWYSIWYG',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group' => 'trwslideroptions',
            'name'  => 'bTRWSliderIndividualBtn',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group' => 'trwslideroptions',
            'name'  => 'sTRWSliderPicSize',
            'type'  => 'str',
            'value' => '1000*400',
        ],
        [
            'group' => 'trwslideroptions',
            'name'  => 'sTRWSliderPicAlternativeSize',
            'type'  => 'str',
            'value' => '500*400',
        ],
    ],
];
