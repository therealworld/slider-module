[{*
    This snippets are only the Template-Framework to access the sliders
    You must complete your appropriate JS slider library by yourself

    The following snippet can be used in this template:
    \tpl\layout\page.tpl
*}]

[{* SNIPPET FOR PAGEHEADER *}]
[{if $oView|method_exists:'getPageHeaderSlider' && $oView->getPageHeaderSlider()}]
    [{assign var="oSlider" value=$oView->getPageHeaderSlider()}]
    [{if $oSlider->getSliderElements()}]
        <ul>
            [{foreach from=$oSlider->getSliderElements() item="oSliderElement" name="PageHeaderSliderElements"}]
            <li [{if $oSliderElement->getBGColor()}]style="background-color:#[{$oSliderElement->getBGColor()}];"[{/if}]>
                <figure>
                    [{if $oSliderElement->getLink()}]<a href="[{$oSliderElement->getLink()}]" [{if $oSliderElement->getLinkTarget()}]target="[{$oSliderElement->getLinkTarget()}]"[{/if}] data-link-type="[{$oSliderElement->getLinkClass()}]">[{/if}]
                    [{if $oSliderElement->getSliderPictureAlternativeUrl() && $oSliderElement->getSliderPictureUrl()}]
                        <picture>
                            <source srcset="[{$oSliderElement->getSliderPictureUrl()}]" media="(min-width: 768px)" />
                            <img src="[{$oSliderElement->getSliderPictureAlternativeUrl()}]" [{if $oSliderElement->getImageTitle()}]alt="[{$oSliderElement->getImageTitle()}]"[{/if}] [{if $oSliderElement->getTitle()}]title="[{$oSliderElement->getTitle()}]"[{/if}] />
                        </picture>
                    [{else}]
                        <img src="[{$oSliderElement->getSliderPictureUrl()}]" [{if $oSliderElement->getImageTitle()}]alt="[{$oSliderElement->getImageTitle()}]"[{/if}] [{if $oSliderElement->getTitle()}]title="[{$oSliderElement->getTitle()}]"[{/if}] />
                    [{/if}]
                    [{if $oSliderElement->getImageTitle()}]
                        <figcaption>[{$oSliderElement->getImageTitle()}]</figcaption>
                    [{/if}]
                    [{if $oSliderElement->getLink()}]</a>[{/if}]
                </figure>
                [{if !$oSliderElement->isFullWidth()}]
                    [{if $oSliderElement->getTitle()}]
                        <h3>[{$oSliderElement->getTitle()}]</h3>
                    [{/if}]
                    [{if $oSliderElement->getDescription()}]
                        <div>[{$oSliderElement->getDescription()}]</div>
                    [{/if}]
                    [{if $oSliderElement->getLink()}]
                        [{assign var="sBtnStyle" value=""}]
                        [{if $oSliderElement->getBtnColor()}]
                            [{assign var="sBtnStyle" value=$sBtnStyle|cat:"color:#"|cat:$oSliderElement->getBtnColor()|cat:"; "}]
                        [{/if}]
                        [{if $oSliderElement->getBtnBGColor()}]
                            [{assign var="sBtnStyle" value=$sBtnStyle|cat:"background-color:#"|cat:$oSliderElement->getBtnBGColor()|cat:"; "}]
                        [{/if}]
                        [{if $sBtnStyle}]
                            [{assign var="sBtnStyle" value='style="'|cat:$sBtnStyle|cat:'"'}]
                        [{/if}]
                        <a class="btn" [{$sBtnStyle}] href="[{$oSliderElement->getLink()}]" [{if $oSliderElement->getLinkTarget()}]target="[{$oSliderElement->getLinkTarget()}]"[{/if}] data-link-type="[{$oSliderElement->getLinkClass()}]">
                            [{assign var="sLinkText" value="MORE_INFO"|oxmultilangassign}]
                            [{$sLinkText}]
                        </a>
                    [{/if}]
                [{/if}]
            </li>
            [{/foreach}]
        </ul>
    [{/if}]
[{/if}]
[{* SNIPPET FOR PAGEHEADER END *}]


[{*
    The following snippets can be used in this templates:
    \tpl\layout\page.tpl

    or individual

    \tpl\page\shop\start.tpl
    \tpl\page\details\details.tpl
    \tpl\page\list\list.tpl
*}]


[{* SNIPPET FOR PAGEHEADER ALTERNATIVE *}]
[{if $oView|method_exists:'getPageHeaderAlternativeSlider' && $oView->getPageHeaderAlternativeSlider()}]
    [{assign var="oSlider" value=$oView->getPageHeaderAlternativeSlider()}]
    [{* ... Sourcecode see above ... *}]
[{/if}]
[{* SNIPPET FOR PAGEHEADER ALTERNATIVE END *}]

[{* SNIPPET FOR MAINHEADER *}]
[{if $oView|method_exists:'getMainHeaderSlider' && $oView->getMainHeaderSlider()}]
    [{assign var="oSlider" value=$oView->getMainHeaderSlider()}]
    [{* ... Sourcecode see above ... *}]
[{/if}]
[{* SNIPPET FOR MAINHEADER END *}]

[{* SNIPPET FOR MAINFOOTER *}]
[{if $oView|method_exists:'getMainFooterSlider' && $oView->getMainFooterSlider()}]
    [{assign var="oSlider" value=$oView->getMainFooterSlider()}]
    [{* ... Sourcecode see above ... *}]
[{/if}]
[{* SNIPPET FOR MAINFOOTER END *}]
