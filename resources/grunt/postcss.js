module.exports = {

    options: {
        processors: [
            require('autoprefixer')({browserlist: ['last 2 versions', 'ie 11']})
        ]
    },
    dist: {
        files: [
            {
                expand: true,
                cwd: '../Application/views/admin/src',
                src: ['*.css', '!*.min.css'],
                dest: '../Application/views/admin/src',
                ext: '.css',
                extDot: 'last'
        }
        ]
    }
};
