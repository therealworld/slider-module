const sass = require('node-sass');

module.exports = {
    moduleproduction: {
        options: {
            implementation: sass,
            update: true,
            style: 'compressed'
        },
        files: {
            "../Application/views/admin/src/slider.css": "build/scss/slider.scss"
        }
    }
};

