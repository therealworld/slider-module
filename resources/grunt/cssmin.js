module.exports = {
    options: {
        mergeIntoShorthands: false,
        roundingPrecision: -1
    },
    target: {
        files: [
            {
                expand: true,
                cwd: '../Application/views/admin/src',
                src: ['*.css', '!*.min.css'],
                dest: '../Application/views/admin/src',
                ext: '.min.css',
                extDot: 'last'
        }
        ]
    }
};
