/**
* move a tablerow up or down
*
* @param obj    oSelf - self
* @param string sClass - name of element that move up or down
* @param string sDir - direction (up/down)
*
* @return null
*/
function moveRow(oSelf, sClass, sDir)
{
    var oSource = oSelf.parentNode.parentNode;
    var oTarget, oTrans;
    if (sDir == 'down') {
        oTarget = oSource.nextElementSibling;
    } else {
        oTarget = oSource.previousElementSibling;
    }
    if (oSource && oTarget) {
        sSource = oSource.getElementsByClassName(sClass)[0].value;
        sTarget = oTarget.getElementsByClassName(sClass)[0].value;
        oSource.getElementsByClassName(sClass)[0].value = sTarget;
        oTarget.getElementsByClassName(sClass)[0].value = sSource;
    }

    submitForm();
}

function deletePicAndSubmit(sField)
{
    deletePic(sField);
    submitForm();
}

function deletePic(sField)
{
    document.getElementById(sField).value = "";
}

function deleteElement(sId)
{
    var oForm = document.getElementById("myedit");
    document.getElementById('DeleteEl').value = sId;
    oForm.fnc.value = "deleteElement";
    oForm.submit();
    return false;
}

function submitForm()
{
    var oForm = document.getElementById("myedit");
    TRWSliderInsert();
    oForm.fnc.value = "save";
    oForm.submit();
    return false;
}
function addNewElement()
{
    var oForm = document.getElementById("myedit");
    TRWSliderInsert();
    oForm.fnc.value = "addNewElement";
    oForm.submit();
    return false;
}